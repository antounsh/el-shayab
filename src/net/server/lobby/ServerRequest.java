package net.server.lobby;

import java.io.Serializable;
import java.util.ArrayList;

public interface ServerRequest extends Serializable, Runnable
{
	public ArrayList<String> getClientNames();
	public int getClientPos();
}
