package net.server.lobby;

import java.util.ArrayList;

public class ServerUpdateLobbyRequest implements ServerRequest
{
	private static final long serialVersionUID = 3058613749884627451L;
	private ArrayList<String> c_names;
	private boolean new_lobby, is_ready;
	private int client_pos;
	private int port;

	public ServerUpdateLobbyRequest(ArrayList<String> client_names, boolean is_new_lobby, boolean is_ready, int client_pos, int new_port)
	{
		c_names = client_names;
		new_lobby = is_new_lobby;
		this.is_ready = is_ready;
		this.client_pos = client_pos;
		port = new_port;
	}

	@Override
	public void run()
	{
	}
	
	public boolean isNewLobby()
	{
		return new_lobby;
	}
	
	public boolean isReady()
	{
		return is_ready;
	}
	
	public int getServerPort()
	{
		return port;
	}
	
	@Override
	public ArrayList<String> getClientNames()
	{
		return c_names;
	}

	@Override
	public int getClientPos()
	{
		return client_pos;
	}

}
