package net.server.lobby;

import java.util.ArrayList;

import game.PlayerType;

public class ServerStartGameRequestAccepted implements ServerRequest
{
	private static final long serialVersionUID = -8730169032645561706L;
	private ArrayList<PlayerType> players;
	private int curr_player;
	private int num_mc;
	
	public ServerStartGameRequestAccepted(ArrayList<PlayerType> players, int current_player, int num_married_cards)
	{
		this.players =  players;
		curr_player = current_player;
		num_mc = num_married_cards;
	}
	
	public int getNumMarriedCards()
	{
		return num_mc;
	}
	
	public int getCurrentPlayer()
	{
		return curr_player;
	}

	public ArrayList<PlayerType> getPlayers()
	{
		return players;
	}
	
	@Override
	public void run()
	{
	}
	
	@Override
	public int getClientPos()
	{
		return 0;
	}

	@Override
	public ArrayList<String> getClientNames()
	{
		return null;
	}
}
