package net.server.lobby;

import java.util.ArrayList;

public class ServerCloseRequestAccepted implements ServerRequest
{
	private static final long serialVersionUID = -795892723656935577L;
	private boolean show_stage;

	public ServerCloseRequestAccepted(boolean show_stage)
	{
		this.show_stage = show_stage;
	}
	
	public boolean isShowStage()
	{
		return show_stage;
	}
	
	@Override
	public void run()
	{
	}

	@Override
	public ArrayList<String> getClientNames()
	{
		return null;
	}

	@Override
	public int getClientPos()
	{
		return 0;
	}

}
