package net.server.lobby;

import java.util.ArrayList;

public class ServerJoinRequestAccepted implements ServerRequest
{
	private static final long serialVersionUID = -2557071821522654678L;
	private ArrayList<String> c_names;
	private int client_pos;
	
	public ServerJoinRequestAccepted(ArrayList<String> client_names, int current_client_pos)
	{
		c_names = client_names;
		client_pos = current_client_pos;
	}

	@Override
	public void run()
	{
	}

	@Override
	public ArrayList<String> getClientNames()
	{
		return c_names;
	}

	@Override
	public int getClientPos()
	{
		return client_pos;
	}

}
