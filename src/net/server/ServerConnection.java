package net.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import game.Player;
import net.client.game.PlayerCardClickedRequest;
import net.client.game.PlayerCloseRequest;
import net.client.game.PlayerEndTurnRequest;
import net.client.lobby.HostCloseRequest;
import net.client.lobby.ClientJoinRequest;
import net.client.lobby.ClientLeaveRequest;
import net.client.lobby.ClientNameChangeRequest;
import net.client.lobby.ClientReadyRequest;
import net.client.lobby.HostStartGameRequest;
import net.client.lobby.ServerJoinRequestRejected;
import net.server.lobby.ServerJoinRequestAccepted;
import net.server.lobby.ServerCloseRequestAccepted;
import net.server.lobby.ServerRequest;
import net.server.lobby.ServerUpdateLobbyRequest;

public class ServerConnection extends Thread
{
	private Socket socket;
	private Server server;
	private ObjectOutputStream send_data;
	private ObjectInputStream incoming_data;
	private Player player;
	private volatile boolean thread_running;
	private boolean is_ready;
	private int connection_pos;
	private String client_name;
	
	public ServerConnection(Socket connection_socket, Server server, String client_name, int connection_pos)
	{
		super("Server Connection Thread");
		thread_running = false;
		is_ready = false;
		socket = connection_socket;
		this.server = server;
		player = null;
		this.client_name = client_name;
		this.connection_pos = connection_pos;
		try
		{
			send_data = new ObjectOutputStream(socket.getOutputStream());
			incoming_data = new ObjectInputStream(socket.getInputStream());
			/*
			ip_address = socket.getRemoteSocketAddress().toString();
			ip_address = ip_address.substring(ip_address.indexOf("/") + 1, ip_address.indexOf(":"));
			String port_temp = socket.getRemoteSocketAddress().toString();
			port_temp = port_temp.substring(port_temp.indexOf(":") + 1);
			//port = Integer.parseInt(port_temp);
			*/
			
			thread_running = true;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void run()
	{
		while (isRunning())
		{
			try
			{
				Object obj = incoming_data.readObject();
				System.out.println("SERVER(" + socket.getLocalSocketAddress() + "): " + "Recieving message from client (" + socket.getRemoteSocketAddress() + "): Object recieved: " + obj);
				
				ServerRequest server_response = null;
				if (obj instanceof ClientJoinRequest)
				{
					ClientJoinRequest data = (ClientJoinRequest) obj;
					String new_client_name = data.getClientName();
					if (server.isValidClientName(new_client_name))
					{
						client_name = new_client_name;
						ArrayList<String> c_names = server.getClientNames();
						server_response = new ServerJoinRequestAccepted(c_names, getConnectionPos());
						
						ArrayList<ServerConnection> connections = server.getServerConnections();
						for (ServerConnection curr_connection : connections)
						{
							if (curr_connection.equals(this)) continue;
							ServerRequest server_request = new ServerUpdateLobbyRequest(c_names, false, curr_connection.isReady(), curr_connection.getConnectionPos(), 0);
							curr_connection.send(server_request);
						}
					}
					else 
					{
						send(new ServerJoinRequestRejected());
						join(10000);
						close();
						server.removeConnection(connection_pos);
					}
				}
				else if (obj instanceof ClientLeaveRequest)
				{
					preClose();
					server.removeConnection(connection_pos);
					ArrayList<ServerConnection> connections = server.getServerConnections();
					ArrayList<String> client_names = server.getClientNames();
					for (ServerConnection curr_connection : connections) curr_connection.send(new ServerUpdateLobbyRequest(client_names, false, curr_connection.isReady(), curr_connection.getConnectionPos(), 0));
					
					server_response = new ServerCloseRequestAccepted(false);
					join(10000);
				}
				else if (obj instanceof ClientNameChangeRequest)
				{
					ClientNameChangeRequest data = (ClientNameChangeRequest) obj;
					String new_client_name = data.getClientName();
					if (server.isValidClientName(new_client_name))
					{
						client_name = new_client_name;
						ArrayList<String> c_names = server.getClientNames();
						ArrayList<ServerConnection> connections = server.getServerConnections();
						for (ServerConnection curr_connection : connections)
						{
							ServerRequest server_request = new ServerUpdateLobbyRequest(c_names, false, curr_connection.isReady(), curr_connection.getConnectionPos(), 0);
							curr_connection.send(server_request);
						}
					}
				}
				else if (obj instanceof HostCloseRequest) 
				{
					if (connection_pos == 0) 
					{
						server.closeMainServer();
						server.preCloseAllServers();
						HostCloseRequest data = (HostCloseRequest) obj;
						server.send(new ServerCloseRequestAccepted(data.isShowStage()));
						join(10000);
						server.closeAllServers();
					}
				}
				else if (obj instanceof ClientReadyRequest)
				{
					setReady();
					server_response = new ServerUpdateLobbyRequest(server.getClientNames(), false, isReady(), getConnectionPos(), 0);
				}
				else if (obj instanceof HostStartGameRequest && server.clientsReady()) 
					server.startGameProcedures();
				else if (obj instanceof PlayerEndTurnRequest)
					server.endPlayerTurn(connection_pos);
				else if (obj instanceof PlayerCardClickedRequest)
				{
					PlayerCardClickedRequest data = (PlayerCardClickedRequest) obj;
					server.moveCard(data.getClickedPlayerPos(), data.getClickedCardPos(), connection_pos);
				}
				else if (obj instanceof PlayerCloseRequest)
				{
					PlayerCloseRequest data = (PlayerCloseRequest) obj;
					if (connection_pos == 0)
					{
						server.preCloseAllServers();
						server.send(new net.server.game.ServerCloseRequestAccepted(data.isShowPreviousMenu()));
						join(10000);
						server.closeAllServers();
					}
					else
					{
						preClose();
						server.removePlayer(this);
						send(new net.server.game.ServerCloseRequestAccepted(data.isShowPreviousMenu()));
					}
				}
				else if (obj instanceof net.client.game.HostRestartRequest)
				{
					if (connection_pos == 0) 
					{
						net.client.game.HostRestartRequest data = (net.client.game.HostRestartRequest) obj;
						if (data.isReturnToServerLobby() || !server.restartGame())
						{
							ArrayList<ServerConnection> connections = server.getServerConnections();
							server = new Server(connections);
							server.start();
							ArrayList<String> client_names = server.getClientNames();
							for (ServerConnection curr_connection : connections) 
							{
								curr_connection.setReady();
								curr_connection.send(new ServerUpdateLobbyRequest(client_names, true, curr_connection.isReady(), curr_connection.getConnectionPos(), server.getPort()));
							}
						}
						
					}
				}
				
				send(server_response);
			} catch (Exception e)
			{
				if (!isRunning()) break;
				
				e.printStackTrace();
			}
		}
		System.out.println("SERVER CONNECTION DONE");
	}
	
	public void setConnectionPos(int connection_position)
	{
		connection_pos = connection_position;
	}
	
	public void setReady()
	{
		is_ready = !is_ready;
	}
	
	public void close()
	{
		try
		{
			if (incoming_data != null)
				incoming_data.close();
			if (send_data != null)
				send_data.close();
			if (socket != null)
				socket.close();
			thread_running = false;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void preClose()
	{
		if (isRunning()) thread_running = false;
	}
	
	public void setClientName(String new_name)
	{
		if (player == null)
			client_name = new_name;
		else
			player.setPlayerName(new_name);
	}
	
	public void setPlayer(Player player)
	{
		this.player = player;
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public boolean send(Object data)
	{
		if (data != null)
		{
			try
			{
				send_data.writeObject(data);
				return true;
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public int getConnectionPos()
	{
		return connection_pos;
	}
	
	public boolean isReady()
	{
		return is_ready;
	}
	
	public boolean isRunning()
	{
		return thread_running;
	}
	
	public String getClientName()
	{
		if (player == null)
			return client_name;
		else
			return player.getPlayerName();
	}
}
