package net.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import game.Card;
import game.Deck;
import game.HiddenPlayer;
import game.Player;
import game.PlayerType;
import game.Players;
import net.server.game.ServerGameOverRequest;
import net.server.game.ServerUpdateBoardRequest;
import net.server.lobby.ServerStartGameRequestAccepted;
import util.NetworkChecker;

public class Server extends Thread
{
	private ServerSocket socket;
	private Players players;
	private volatile ArrayList<ServerConnection> connections;
	private ArrayList<Card> shuffled_deck;
	private volatile boolean server_running;
	private volatile boolean curr_player_done;
	private boolean testing = false;
	private static int port_low = 1200;
	private static int port_high = 1500;
	private int port;
	
	public Server()
	{
		super("Main Server Thread");
		server_running = false;
		curr_player_done = false;
		port = NetworkChecker.getOpenPort(port_low, port_high);
		port = 1234; // for testing purposes
		connections = new ArrayList<ServerConnection>();
		players = new Players();
		shuffled_deck = new ArrayList<Card>();
		try
		{
			socket = new ServerSocket(port);
			server_running = true;
		} catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public Server(ArrayList<ServerConnection> old_connections)
	{
		super("Main Server Thread");
		server_running = false;
		curr_player_done = false;
		port = NetworkChecker.getOpenPort(port_low, port_high);
		connections = old_connections;
		players = new Players();
		shuffled_deck = new ArrayList<Card>();
		try
		{
			socket = new ServerSocket(port);
			server_running = true;
		} catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void run()
	{
		while(isRunning())
		{
			try
			{
				while (numOfClients() == 4);
				Socket connection_socket = socket.accept();
				
				String client_name = getValidClientName();
				ServerConnection new_connection = new ServerConnection(connection_socket, this, client_name, numOfClients());
				connections.add(new_connection);
				new_connection.start();
			} catch (Exception e)
			{
				if (!isRunning()) break;
				
				e.printStackTrace();
			}
		}
		
		System.out.println("MAIN SERVER IS DONE");
	}
	
	public void startGameProcedures()
	{
		closeMainServer();
		int num_players = numOfClients();
		for (int i = 0; i < num_players; i++)
		{
			ServerConnection connection = connections.get(i);
			Player player = new Player(connection.getClientName());
			players.addPlayer(player);
			connection.setPlayer(player);
		}
		players.shufflePlayers();
		
		Deck deck = new Deck();
		shuffled_deck = deck.getShuffledDeck();
		int i = 0;
		while(!shuffled_deck.isEmpty())
		{
			if (i == num_players) i = 0;
			
			Player p = players.getPlayer(i);
			p.addCard(shuffled_deck.remove(0));
			i++;
		}
		turnOver(true, true);	
	}
	
	public void closeMainServer()
	{	
		try
		{
			server_running = false;
			while (server_running);
			
			if (socket != null)
				socket.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void preCloseAllServers()
	{
		for (ServerConnection curr_connection : connections)
		{
			curr_connection.preClose();
			while (curr_connection.isRunning());
		}
	}
	
	public void closeAllServers()
	{
		try
		{
			for (ServerConnection curr_connection : connections) curr_connection.close();
			
			closeMainServer();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void removeConnection(int connection_position)
	{
		connections.remove(connection_position);
		int size = numOfClients();
		for (int i = 0; i < size; i++)
		{
			ServerConnection curr_connection = connections.get(i);
			curr_connection.setConnectionPos(i);
		}
	}
	
	public void removePlayer(ServerConnection server_connection)
	{
		removeConnection(connections.indexOf(server_connection));
		Player player = server_connection.getPlayer();
		int num_players_before = players.numOfPlayers();
		int removed_player_pos = players.removePlayerCompletely(player);
		
		// otherwise player already removed or game ended
		if (removed_player_pos != -1 && num_players_before > 1)
		{
			// the remaining player is the winner
			if (num_players_before == 2) playerFinished(-1);
			// at least 2 players are left after
			else
			{
				int curr_player_pos = players.getCurrentPlayerPosition();
				if (curr_player_pos == removed_player_pos)
				{
					if (curr_player_pos == (num_players_before - 1)) players.nextPlayer();
					curr_player_done = false;
					turnOver(false, true);
				}
				else
				{
					// removed player can be before or after current player
					// 0(R) 	1(C) 	2 	3
					if (removed_player_pos < curr_player_pos) players.setCurrentPlayerPosition(--curr_player_pos);
					turnOver(false, false);
				}
			}
		}
	}
	
	public void moveCard(int player_a_pos, int player_a_card_pos, int connection_pos)
	{
		int curr_player_pos = players.getCurrentPlayerPosition();
		int next_player_pos = curr_player_pos + 1;
		if (next_player_pos == players.numOfPlayers())
			next_player_pos = 0;
		
		Player player_b = connections.get(connection_pos).getPlayer();
		Player curr_player = players.getPlayer(curr_player_pos);
		if ((next_player_pos == player_a_pos) && curr_player.equals(player_b) && !curr_player_done)
		{
			curr_player_done = true;
			Player next_player = players.getPlayer(next_player_pos);
			curr_player.addCard(next_player.removeCard(player_a_card_pos));
			curr_player.marryCards();
			turnOver(false, false);
			
			if (curr_player.numberOfCards() == 0)
			{
				playerFinished(curr_player_pos);
				next_player_pos--;
				if (next_player_pos < 0)
					next_player_pos = 0;
			}
			
			if (next_player.numberOfCards() == 0) playerFinished(next_player_pos);
		}
	}
	
	public void endPlayerTurn(int connection_pos)
	{
		if (!curr_player_done) return;
		
		ServerConnection connection = connections.get(connection_pos);
		int curr_player_pos = players.getCurrentPlayerPosition();
		Player curr_player = players.getPlayer(curr_player_pos);
		if (!curr_player.equals(connection.getPlayer())) return;
		
		curr_player_done = false;
		players.nextPlayer();
		turnOver(false, true);
	}
	
	public ArrayList<ServerConnection> getServerConnections()
	{
		return connections;
	}
	
	public ArrayList<String> getClientNames()
	{
		ArrayList<String> client_names = new ArrayList<String>();
		for (ServerConnection curr_connection : connections) client_names.add(curr_connection.getClientName());
		
		return client_names;
	}
	
	public String getPublicIPAddress()
	{
		try
		{
			return NetworkChecker.getIP();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public int getPort()
	{
		return port;
	}
	
	public int numOfClients()
	{
		return connections.size();
	}
	
	public String getValidClientName()
	{
		ArrayList<String> c_names = getClientNames();
		int num = numOfClients() + 1;
		String client_name = "Player " + num + "'s Name";
		while (c_names.contains(client_name)) client_name = "Player " + ++num + "'s Name";
		return client_name;
	}
	
	public boolean isValidClientName(String client_name)
	{
		if (client_name == null || client_name.length() < 1) return false;
		
		boolean result = true;
		ArrayList<String> c_names = getClientNames();
		if (c_names.contains(client_name)) result = false;
		return result;
	}
	
	public boolean clientsReady()
	{
		int size = numOfClients();
		if (size < 2)
			return false;
		boolean clients_ready = true;
		for (int i = 1; i < size; i++)
		{
			ServerConnection curr_connection = connections.get(i);
			if (!curr_connection.isReady())
				clients_ready = false;
		}
		return clients_ready;
	}
	
	public boolean hasConnections()
	{
		boolean result = false;
		int size = numOfClients();
		for (int i = 0; i < size; i++)
		{
			ServerConnection curr_connection = connections.get(i);
			if (curr_connection.isRunning())
				result = true;
		}
		return result;
	}
	
	public boolean isRunning()
	{
		return server_running;
	}
	
	public boolean send(Object data)
	{
		if (data == null) return false;
		
		for (ServerConnection curr_connection : connections) curr_connection.send(data);
		return true;
	}
	
	public boolean restartGame()
	{
		if (numOfClients() < 2) return false;
		
		players = new Players();
		curr_player_done = false;
		startGameProcedures();
		return true;
	}
	
	private void turnOver(boolean start_game, boolean end_of_turn)
	{
		if (end_of_turn) 
		{
			players.marryAllCards();
			players.shuffleAllHands();
		}
		
		int curr_player = players.getCurrentPlayerPosition();
		int num_mc = players.numberOfMarriedCards();
		int num_clients = numOfClients();
		int num_players = players.numOfPlayers();
		for (int j = 0; j < num_clients; j++)
		{
			ArrayList<PlayerType> temp_players = new ArrayList<PlayerType>();
			ServerConnection connection = connections.get(j);
			for (int i = 0; i < num_players; i++)
			{
				PlayerType player = players.getPlayer(i);
				if (!player.equals(connection.getPlayer())) player = new HiddenPlayer(player.getPlayerName(), player.numberOfCards());
				else player = new Player((Player) player);
				
				temp_players.add(player);
			}
			if (start_game) connection.send(new ServerStartGameRequestAccepted(temp_players, curr_player, num_mc));
			else connection.send(new ServerUpdateBoardRequest(temp_players, curr_player, num_mc));
		}
		
		// only for testing, ends the game right away
		if (testing) 
		{
			//turnOver(false, false);
			playerFinished(0);
			testing = false;
		}
	}
	
	private void playerFinished(int player_pos)
	{
		if (player_pos != -1) players.removePlayer(player_pos);
		
		int new_player_size = players.numOfPlayers();
		if (new_player_size == 1)
		{
			curr_player_done = false;
			Player loser = players.getPlayer(0);
			ArrayList<Player> orig_players = players.getOrignalPlayers();
	        int orig_players_size = orig_players.size();
	        String winners = "";
	        if (player_pos != -1)
	        {
	        	for (int i = 0; i < orig_players_size; i++)
		        {
		        	Player curr_player = orig_players.get(i);
		        	if (loser.equals(curr_player))
		        		continue;
		        	if (winners.isEmpty())
		        		winners += curr_player.getPlayerName();
		        	else
		        		winners += "\n" + curr_player.getPlayerName();
		        }
	        }
	        else winners = players.getPlayer(0).getPlayerName();
	        	        
	        int size = numOfClients();
	        for (int i = 0; i < size; i++) connections.get(i).send(new ServerGameOverRequest(winners, orig_players_size));
		}
	}
}
