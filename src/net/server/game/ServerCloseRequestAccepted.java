package net.server.game;

import java.util.ArrayList;

import net.server.lobby.ServerRequest;

public class ServerCloseRequestAccepted implements ServerRequest
{
	private static final long serialVersionUID = 4624778270268863367L;
	private boolean show_previous_menu;
	
	public ServerCloseRequestAccepted(boolean show_previous_menu)
	{
		this.show_previous_menu = show_previous_menu;
	}
	
	public boolean isShowPreviousMenu()
	{
		return show_previous_menu;
	}
	
	@Override
	public void run()
	{
	}

	@Override
	public ArrayList<String> getClientNames()
	{
		return null;
	}

	@Override
	public int getClientPos()
	{
		return 0;
	}

}
