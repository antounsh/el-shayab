package net.server.game;

import java.util.ArrayList;

import net.server.lobby.ServerRequest;

public class ServerGameOverRequest implements ServerRequest
{
	private static final long serialVersionUID = 6298123533231229801L;
	private String winners;
	private int orig_players_size;

	public ServerGameOverRequest(String winners, int original_players_size)
	{
		this.winners = winners;
		orig_players_size = original_players_size;
	}
	
	public String getWinners()
	{
		return winners;
	}
	
	public int getOriginalPlayersSize()
	{
		return orig_players_size;
	}
	
	@Override
	public void run()
	{
	}
	
	@Override
	public int getClientPos()
	{
		return 0;
	}

	@Override
	public ArrayList<String> getClientNames()
	{
		return null;
	}
}
