package net.server.game;

import java.util.ArrayList;

import game.PlayerType;
import net.server.lobby.ServerRequest;

public class ServerUpdateBoardRequest implements ServerRequest
{
	private static final long serialVersionUID = -8730169032645561706L;
	private ArrayList<PlayerType> players;
	private int curr_player_pos;
	private int num_mc;
	
	public ServerUpdateBoardRequest(ArrayList<PlayerType> players, int current_player_position, int num_married_cards)
	{
		this.players = players;
		curr_player_pos = current_player_position;
		num_mc = num_married_cards;
	}
	
	public int getNumMarriedCards()
	{
		return num_mc;
	}
	
	public int getCurrentPlayerPos()
	{
		return curr_player_pos;
	}

	public ArrayList<PlayerType> getPlayers()
	{
		return players;
	}
	
	@Override
	public void run()
	{
	}
	
	@Override
	public int getClientPos()
	{
		return 0;
	}

	@Override
	public ArrayList<String> getClientNames()
	{
		return null;
	}
}
