package net.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import gui.ServerLobby;
import javafx.application.Platform;
import javafx.stage.Stage;
import net.client.game.ClientRestartGameProcedures;
import net.client.game.GameFinishedAlert;
import net.client.lobby.ClientJoinRequest;
import net.client.lobby.ClientRequest;
import net.client.lobby.ServerJoinRequestRejected;
import net.server.game.ServerGameOverRequest;
import net.server.game.ServerUpdateBoardRequest;
import net.server.lobby.ServerJoinRequestAccepted;
import net.server.lobby.ServerCloseRequestAccepted;
import net.server.lobby.ServerStartGameRequestAccepted;
import net.server.lobby.ServerUpdateLobbyRequest;

public class Client extends Thread
{
	private ObjectInputStream incoming_data;
	private ObjectOutputStream send_data;
	private Socket server_connection;
	private ServerLobby server_lobby;
	private OnlineGameController game_controller;
	private String ip;
	private int port;
	private volatile boolean client_running;
	
	public Client(int server_port, String server_ip, String player_name, ServerLobby server_lobby)
	{
		super("Main Client Thread");
		client_running = false;
		port = server_port;
		ip = server_ip;
		this.server_lobby = server_lobby;
		game_controller = null;
		
		try
		{
			server_connection = new Socket(ip, port);
			send_data = new ObjectOutputStream(server_connection.getOutputStream());
			incoming_data = new ObjectInputStream(server_connection.getInputStream());
			client_running = true;
			
			ClientRequest req = new ClientJoinRequest(player_name);
			send(req);
		} catch (Exception e)
		{
			client_running = false;
		}
	}
	
	@Override
	public void run()
	{
		while (isConnected())
		{
			try
			{
				Object obj = incoming_data.readObject();
				System.out.println("CLIENT(" + server_connection.getLocalSocketAddress() + "): " + "Recieving message from server_connection (" + server_connection.getRemoteSocketAddress() + "): Object recieved: " + obj);
				ClientRequest client_response = null;
				if (obj instanceof ServerJoinRequestAccepted)
				{
					ServerJoinRequestAccepted data = (ServerJoinRequestAccepted) obj;
					server_lobby.setClientPos(data.getClientPos());
					server_lobby.setClientNames(data.getClientNames());
				}
				else if (obj instanceof ServerJoinRequestRejected) closeClient(true, false);
				else if (obj instanceof ServerUpdateLobbyRequest)
				{
					ServerUpdateLobbyRequest data = (ServerUpdateLobbyRequest) obj;
					if (data.isNewLobby())
					{
						port = data.getServerPort();
						server_lobby.updateServerIP(ip, port);
						server_lobby.setClientPos(data.getClientPos());
						server_lobby.setClientNames(data.getClientNames());
						server_lobby.setReady(data.isReady());
						Platform.runLater(server_lobby);
					}
					else
					{
						server_lobby.setClientPos(data.getClientPos());
						server_lobby.setClientNames(data.getClientNames());
						server_lobby.setReady(data.isReady());
					}
				}
				else if (obj instanceof ServerCloseRequestAccepted)
				{
					ServerCloseRequestAccepted data = (ServerCloseRequestAccepted) obj;
					closeClient(data.isShowStage(), true);
				}
				else if (obj instanceof ServerStartGameRequestAccepted)
				{
					ServerStartGameRequestAccepted data = (ServerStartGameRequestAccepted) obj;
					if (game_controller == null) game_controller = server_lobby.startGame(data);
					else
					{
						game_controller.setPlayerNames();
						game_controller.setPlayers(data.getPlayers());
						game_controller.setCurrPlayerPos(data.getCurrentPlayer());
						game_controller.setNumMarriedCards(data.getNumMarriedCards());
						Stage stage = game_controller.getStage();
						Platform.runLater(new ClientRestartGameProcedures(game_controller, stage));
					}
				}
				else if (obj instanceof ServerUpdateBoardRequest)
				{
					ServerUpdateBoardRequest data = (ServerUpdateBoardRequest) obj;
					game_controller.setPlayers(data.getPlayers());
					game_controller.setCurrPlayerPos(data.getCurrentPlayerPos());
					game_controller.setNumMarriedCards(data.getNumMarriedCards());
					game_controller.displayBoard();
				}
				else if (obj instanceof ServerGameOverRequest)
				{
					ServerGameOverRequest data = (ServerGameOverRequest) obj;
					Platform.runLater(new GameFinishedAlert(game_controller, data.getWinners(), data.getOriginalPlayersSize()));
				}
				else if (obj instanceof net.server.game.ServerCloseRequestAccepted) 
				{
					net.server.game.ServerCloseRequestAccepted data = (net.server.game.ServerCloseRequestAccepted) obj;
					closeClient(data.isShowPreviousMenu(), true);
				}
				
				send(client_response);
			} catch (Exception e)
			{
				if (!client_running) break;
				
				e.printStackTrace();
			}
		}
		System.out.println("CLIENT CONNECTION DONE");
	}
	
	public String getPublicIPAddressAndPort()
	{
		return server_lobby.getPublicIPAddressAndPort();
	}
	
	public boolean isConnected()
	{
		return client_running;
	}
	
	public boolean isHost()
	{
		return server_lobby.isHost();
	}
	
	public boolean send(Object data)
	{
		if (client_running && data != null)
		{
			try
			{
				send_data.writeObject(data);
				return true;
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	private void closeClient(boolean show_previous_menu, boolean show_close_alert)
	{
		client_running = false;
		server_lobby.setShowPreviousMenu(show_previous_menu);
		server_lobby.showAlert(show_close_alert);
		try
		{
			if (incoming_data != null) incoming_data.close();
			if (send_data != null) send_data.close();
			if (server_connection != null) server_connection.close();
			
			if (show_close_alert) join(10000);
			server_lobby.hideAlert();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
