package net.client;

import java.util.ArrayList;

import game.*;
import gui.GameController;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Stage;
import net.client.game.*;

public class OnlineGameController extends GameController
{
	private Client client;
	private ArrayList<PlayerType> players;
	private int curr_player_pos;
	private int player_pos;
	private int num_mcs;
	
	public OnlineGameController(Stage main_stage, ArrayList<String> player_names, ArrayList<PlayerType> players, Client client, int current_player_position, int num_married_cards)
	{
		super(player_names, true, main_stage);
		stage = main_stage;
		this.players = players;
		int size = players.size();
		for (int i = 0; i < size; i++)
		{
			if (players.get(i) instanceof HiddenPlayer) continue;
			player_pos = i;
		}
		
		this.client = client;
		curr_player_pos = current_player_position;
		num_mcs = num_married_cards;
		Deck d = new Deck();
		deck = d.getDeck();
	}
	
	public void setPlayerNames()
	{
		p_names = new ArrayList<String>();
		for (PlayerType curr_player : players) p_names.add(curr_player.getPlayerName());
	}
	
	public void setPlayers(ArrayList<PlayerType> players)
	{
		this.players = players;
		int size = players.size();
		for (int i = 0; i < size; i++)
		{
			if (players.get(i) instanceof HiddenPlayer) continue;
			player_pos = i;
		}
	}
	
	public void setCurrPlayerPos(int curr_player_pos)
	{
		this.curr_player_pos = curr_player_pos;
	}
	
	public void setNumMarriedCards(int num_married_cards)
	{
		num_mcs = num_married_cards;
	}
	
	public void gameFinishedAlert(String winners, int orig_players_size)
	{
        HBox buttons_organizer = setupGameFinishedAlert(winners);
        Button restart = (Button) buttons_organizer.getChildren().get(0);
        restart.setOnAction(event -> client.send(new HostRestartRequest(false)));
        
        Button exit = (Button) buttons_organizer.getChildren().get(1);
        exit.setOnAction(event -> {
        	event.consume();
        	exitGame();
        });
        if (!client.isHost()) buttons_organizer.getChildren().remove(0);
        
        stage.setOnCloseRequest(event -> {
        	event.consume();
        	exitGame();
        });
        stage.show();
	}
	
	public void changeToMainScene()
	{
		Scene main_scene = gp.getScene();
		stage.hide();
		setStageTitle();
		stage.setScene(main_scene);
	}
	
	public Stage getStage()
	{
		return stage;
	}
	
	@Override
	public void displayBoard()
	{
		int num_players = players.size();
		p_indexes = new int[num_players];
		ObservableList<Node> vbs = gp.getChildren();
		for (int i = 0; i < num_players; i++)
		{
			VBox curr_vb = (VBox) vbs.get(i);
			ObservableList<Node> nodes = curr_vb.getChildren();
			for (Node curr_node : nodes) curr_node.setVisible(true);
			
			HBox curr_hb = (HBox) curr_vb.getChildren().get(0);
			Text curr_name = (Text) curr_hb.getChildren().get(0);
			
			PlayerType player = players.get(i);
			curr_name.setText(player.getPlayerName());
			displayPlayerCards(i, p_indexes[i], false, false);
			unhighlightPlayer(i);
		}
		
		for (int i = num_players; i < 4; i++)
		{
			VBox curr_vb = (VBox) vbs.get(i);
			ObservableList<Node> nodes = curr_vb.getChildren();
			for (Node curr_node : nodes) curr_node.setVisible(false);
		}
		
		total_mcs.setText(Integer.toString(num_mcs));
		highlightPlayer(curr_player_pos);
	}
	
	@Override
	public void endCurrPlayerTurn()
	{
		client.send(new PlayerEndTurnRequest());
	}
	
	@Override
    public void displayOptionsScreen()
    {
    	double width = 300.0, height = 150.0;
    	if (client.isHost()) height = 220.0;
    	VBox options_menu = setupOptionsScreen(width, height);
    	
    	options_menu.getChildren().clear();
		Text server_info = new Text("Server IP: " + client.getPublicIPAddressAndPort());
		server_info.setFont(new Font(20));
		options_menu.getChildren().add(server_info);
		
    	Button btn = new Button("Return to Game");
		btn.setDefaultButton(true);
		btn.setCancelButton(true);
		btn.setTextAlignment(TextAlignment.CENTER);
		btn.setOnAction(event -> {
			changeToMainScene();
			stage.show();
		});
    	options_menu.getChildren().add(btn);
    	
    	if (client.isHost())
    	{	
	    	btn = new Button("Restart Game");
			btn.setDefaultButton(false);
			btn.setCancelButton(false);
			btn.setTextAlignment(TextAlignment.CENTER);
			btn.setOnAction(event -> client.send(new HostRestartRequest(false)));
	    	options_menu.getChildren().add(btn);
	    	
	    	btn = new Button("Return to Server Lobby");
			btn.setDefaultButton(false);
			btn.setCancelButton(false);
			btn.setTextAlignment(TextAlignment.CENTER);
			btn.setOnAction(event -> client.send(new HostRestartRequest(true)));
	    	options_menu.getChildren().add(btn);
    	}
    	
    	btn = new Button("Exit Server");
		btn.setDefaultButton(false);
		btn.setCancelButton(false);
		btn.setTextAlignment(TextAlignment.CENTER);
		btn.setOnAction(event -> client.send(new PlayerCloseRequest(true)));
    	options_menu.getChildren().add(btn);
    	
    	btn = new Button("Exit Server & Game");
		btn.setDefaultButton(false);
		btn.setCancelButton(false);
		btn.setTextAlignment(TextAlignment.CENTER);
		btn.setOnAction(event -> exitGame());
    	options_menu.getChildren().add(btn);
    	stage.show();
    }
	
	@Override
	protected void exitGame()
	{
		client.send(new PlayerCloseRequest(false));
	}
	
	@Override
	protected void unhighlightPlayer(int player_pos)
	{
		VBox p_vb = (VBox) gp.getChildren().get(player_pos);
		HBox p_hb = (HBox) p_vb.getChildren().get(0);
		Text p_name = (Text) p_hb.getChildren().get(0);
		
		String new_name = players.get(player_pos).getPlayerName();
		p_name.setFill(Color.BLACK);
		p_name.setText(new_name);
	}
	
	@Override
	protected void highlightPlayer(int player_pos)
	{
		VBox p_vb = (VBox) gp.getChildren().get(player_pos);
		HBox p_hb = (HBox) p_vb.getChildren().get(0);
		Text p_name = (Text) p_hb.getChildren().get(0);
		
		String new_name = "*** " + p_name.getText() + " ***";
		p_name.setText(new_name);
		p_name.setFill(Color.RED);
		
		player_pos--;
		if (player_pos < 0) player_pos = players.size() - 1;
		unhighlightPlayer(player_pos);
	}
	
	@Override
	protected void playerPrevNextPressed(int player_pos, boolean next_pressed)
	{
		if (next_pressed) p_indexes[player_pos] += 9;
		else p_indexes[player_pos] -= 9;
		
		boolean show_front = false;
		if (this.player_pos == player_pos) show_front = true;
		
		displayPlayerCards(player_pos, p_indexes[player_pos], show_front, false);
	}
	
	@Override
	protected void cardImageClicked(int player_pos, int img_pos)
	{
		int next_player_pos = curr_player_pos + 1;
		if (next_player_pos == players.size()) next_player_pos = 0;
		
		if (next_player_pos == player_pos && this.player_pos != player_pos) client.send(new PlayerCardClickedRequest(player_pos, (p_indexes[player_pos] + img_pos)));
	}
	
	@Override
	protected int displayPlayerCardsHelper(int player_pos, int hand_pos, boolean show_front, ArrayList<ImageView> imgs)
	{
		PlayerType player = players.get(player_pos);
		
		int p_hand_size = player.numberOfCards();
		int num_displayed_imgs = 0;
		if (player instanceof Player)
		{
			if (curr_player_pos == player_pos) end_turn_btn.setVisible(true);
			else end_turn_btn.setVisible(false);
			
			Player p = (Player) player;
			ArrayList<Card> p_hand = p.getHand();
			for (int i = hand_pos; i < p_hand_size; i++)
			{
				if (num_displayed_imgs == 9) break;
				
				ImageView img = imgs.get(num_displayed_imgs);
				Card c = p_hand.get(i);
				int card_num = c.getNumber() - 1;
				String card_suit = c.getSuit();
				
				if (card_suit.compareTo("Hearts") == 0) card_num += 13;
				else if (card_suit.compareTo("Clubs") == 0) card_num += 13 + 12;
				else if (card_suit.compareTo("Diamonds") == 0) card_num += 13 + 12 + 12;
				
				c = deck.get(card_num);
				img.setImage(c.getFrontImage());
				img.setVisible(true);
				num_displayed_imgs++;
			}
		}
		else
		{
			Card c = deck.get(0);
			Image back_img = c.getBackImage();
			for (int i = hand_pos; i < p_hand_size; i++)
			{
				if (num_displayed_imgs == 9) break;
				
				ImageView img = imgs.get(num_displayed_imgs);
				img.setImage(back_img);
				img.setVisible(true);
				num_displayed_imgs++;
			}
		}
		
		return p_hand_size;
	}
}
