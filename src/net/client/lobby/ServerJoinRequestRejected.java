package net.client.lobby;

import java.util.ArrayList;

import net.server.lobby.ServerRequest;

public class ServerJoinRequestRejected implements ServerRequest
{
	private static final long serialVersionUID = 7086340751891780361L;
	
	@Override
	public void run()
	{
	}

	@Override
	public ArrayList<String> getClientNames()
	{
		return null;
	}

	@Override
	public int getClientPos()
	{
		return 0;
	}

}
