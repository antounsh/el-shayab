package net.client.lobby;

public class HostCloseRequest implements ClientRequest
{
	private static final long serialVersionUID = 4557799485789486795L;
	private boolean show_stage;

	public HostCloseRequest(boolean show_stage)
	{
		this.show_stage = show_stage;
	}
	
	public boolean isShowStage()
	{
		return show_stage;
	}
}
