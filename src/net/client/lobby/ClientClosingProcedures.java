package net.client.lobby;

import gui.ServerLobby;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ClientClosingProcedures implements Runnable
{
	private boolean pre_close, show_close_alert;
	private Stage main_stage;
	private Stage close_alert_stage, file_alert_stage;
	private Alert close_alert, file_alert;
	private volatile ServerLobby server_lobby;
	
	public ClientClosingProcedures(Stage main_stage)
	{
		pre_close = true;
		show_close_alert = true;
		server_lobby = null;
		this.main_stage = main_stage;
		
		file_alert = new Alert(AlertType.ERROR);
		file_alert.setHeaderText("Invalid player name!");
		file_alert.setContentText("Please change your name in the settings.");
		
		close_alert = new Alert(AlertType.NONE);
		String new_title = this.main_stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
        close_alert.setTitle(new_title + "Shutting Down");
        close_alert.setHeaderText("Shutting Down the server/client");
        close_alert.setContentText("Please be patient...");
        
        close_alert_stage = (Stage) close_alert.getDialogPane().getScene().getWindow();
        close_alert_stage.getIcons().add(this.main_stage.getIcons().get(0));
        
        file_alert_stage = (Stage) file_alert.getDialogPane().getScene().getWindow();
        file_alert_stage.getIcons().add(this.main_stage.getIcons().get(0));
	}
	
	public void setShowCloseAlert(boolean show_close_alert)
	{
		this.show_close_alert = show_close_alert;
	}
	
	public void setPreClose(boolean pre_close)
	{
		this.pre_close = pre_close;
	}
	
	public void setServerLobby(ServerLobby server_lobby)
	{
		this.server_lobby = server_lobby;
	}

	@Override
	public void run()
	{
		if (pre_close)
		{
			if (show_close_alert)
			{
				main_stage.hide();
				close_alert.show();
			}
		}
		else
		{
			if (show_close_alert) close_alert_stage.close();
			if (server_lobby != null)
			{
				server_lobby.previousPreviousMenu();
				main_stage.show();
			}
			else main_stage.close();
			
			if (!show_close_alert)
			{
				show_close_alert = true;
				file_alert.show();
			}
		}
	}
}