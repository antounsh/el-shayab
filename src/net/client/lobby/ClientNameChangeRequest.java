package net.client.lobby;

public class ClientNameChangeRequest implements ClientRequest
{
	private static final long serialVersionUID = 4839758655492382039L;
	private String c_name;
	
	public ClientNameChangeRequest(String new_client_name)
	{
		c_name = new_client_name;
	}
	
	public String getClientName()
	{
		return c_name;
	}
}
