package net.client.lobby;

public class ClientJoinRequest implements ClientRequest
{
	private static final long serialVersionUID = -6941765222573364778L;
	private String c_name;
	
	public ClientJoinRequest(String client_name)
	{
		c_name = client_name;
	}
	
	public String getClientName()
	{
		return c_name;
	}
}
