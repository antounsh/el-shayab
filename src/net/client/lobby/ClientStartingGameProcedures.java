package net.client.lobby;

import java.util.ArrayList;

import game.PlayerType;
import javafx.stage.Stage;
import net.client.Client;
import net.client.OnlineGameController;
import net.server.lobby.ServerStartGameRequestAccepted;

public class ClientStartingGameProcedures implements Runnable
{
	private Stage stage;
	private ArrayList<String> p_names;
	private ArrayList<PlayerType> players;
	private Client client;
	private ServerStartGameRequestAccepted data;
	private OnlineGameController ogc;
	private volatile boolean is_done;
	
	public ClientStartingGameProcedures(Stage main_stage, ArrayList<String> p_names, ArrayList<PlayerType> players, Client client, ServerStartGameRequestAccepted data)
	{
		is_done = false;
		stage = main_stage;
		this.p_names = p_names;
		this.players = players;
		this.client = client;
		this.data = data;
		ogc = null;
	}
	
	public OnlineGameController getGameController()
	{
		return ogc;
	}

	public boolean isDone()
	{
		return is_done;
	}
	
	@Override
	public void run()
	{
		try
		{
			stage.hide();
			ogc = new OnlineGameController(stage, p_names, players, client, data.getClientPos(), data.getNumMarriedCards());
			ogc.loadSettings(ogc);
			
			stage.show();
			is_done = true;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
