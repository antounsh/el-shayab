package net.client.game;

import net.client.lobby.ClientRequest;

public class PlayerCloseRequest implements ClientRequest
{
	private static final long serialVersionUID = 998644562357097425L;
	private boolean show_previous_menu;

	public PlayerCloseRequest(boolean show_previous_menu)
	{
		this.show_previous_menu = show_previous_menu;
	}
	
	public boolean isShowPreviousMenu()
	{
		return show_previous_menu;
	}
}
