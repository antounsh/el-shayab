package net.client.game;

import javafx.stage.Stage;
import net.client.OnlineGameController;

public class ClientRestartGameProcedures implements Runnable
{
	private OnlineGameController ogc;
	private Stage stage;
	
	public ClientRestartGameProcedures(OnlineGameController online_game_controller, Stage main_stage)
	{
		ogc = online_game_controller;
		this.stage = main_stage;
	}
	
	@Override
	public void run()
	{
		stage.hide();
		ogc.initialSetup();
		ogc.changeToMainScene();
		stage.show();
	}
}
