package net.client.game;

import javafx.stage.Stage;

public class PlayerClosingProcedures implements Runnable
{
	private Stage stage;
	boolean close_application;
	
	public PlayerClosingProcedures(Stage main_stage, boolean close_entire_application)
	{
		stage = main_stage;
		close_application = close_entire_application;
	}
	
	@Override
	public void run()
	{
		if (close_application) stage.close();
		else stage.hide();
	}

}
