package net.client.game;

import net.client.OnlineGameController;

public class GameFinishedAlert implements Runnable
{
	private OnlineGameController ogc;
	private String winners;
	private int orig_players_size;
	
	public GameFinishedAlert(OnlineGameController online_game_controller, String winners, int original_players_size)
	{
		ogc = online_game_controller;
		this.winners = winners;
		orig_players_size = original_players_size;
	}
	
	@Override
	public void run()
	{
		ogc.gameFinishedAlert(winners, orig_players_size);
	}

}
