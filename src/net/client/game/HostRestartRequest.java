package net.client.game;

import net.client.lobby.ClientRequest;

public class HostRestartRequest implements ClientRequest
{
	private static final long serialVersionUID = -929565454590920500L;
	private boolean return_to_server_lobby;
	
	public HostRestartRequest(boolean return_to_server_lobby)
	{
		this.return_to_server_lobby = return_to_server_lobby;
	}
	
	public boolean isReturnToServerLobby()
	{
		return return_to_server_lobby;
	}
}
