package net.client.game;

import net.client.lobby.ClientRequest;

public class PlayerCardClickedRequest implements ClientRequest
{
	private static final long serialVersionUID = 8584086171148992149L;
	private int player_pos, card_pos;
	
	public PlayerCardClickedRequest(int clicked_player_pos, int clicked_card_pos)
	{
		player_pos = clicked_player_pos;
		card_pos = clicked_card_pos;
	}
	
	public int getClickedPlayerPos()
	{
		return player_pos;
	}
	
	public int getClickedCardPos()
	{
		return card_pos;
	}
}
