package gui;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public interface Menu extends Runnable
{
	public void previousMenu();
	public void changeImage(ImageView img_space, Image new_img);
}
