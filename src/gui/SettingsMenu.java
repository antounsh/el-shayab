package gui;

import java.util.ArrayList;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import util.SettingsLoader;

public class SettingsMenu implements Menu
{
	private VBox parent;
	private HBox hbox, hbox1;
	private Text p_name_t, select_reso_t, chars_remaining_t;
	private TextField p_name_tf;
	private ComboBox<String> resolutions;
	private ImageView prev_menu, save_settings;
	private Stage stage;
	private Menu previous_menu;
	private Alert file_error_alert;
	private SettingsLoader settings_loader;
	private String prev_selected_reso;
	private String prev_p_name;
	
	public SettingsMenu(VBox stage_parent, Menu previous_menu)
	{
		parent = stage_parent;
		stage = (Stage) stage_parent.getScene().getWindow();
		this.previous_menu = previous_menu;
		hbox = (HBox) parent.getChildren().get(0);
		hbox1 = (HBox) parent.getChildren().get(1);
		prev_selected_reso = null;
		prev_p_name = null;
		file_error_alert = new Alert(AlertType.ERROR);
		file_error_alert.setContentText("Error saving to file...");
		settings_loader = new SettingsLoader(GameController.user_config_loc, GameController.game_config_loc);
		
		chars_remaining_t = new Text("15 Characters Remaining");
		chars_remaining_t.setFont(new Font(15.0));
		p_name_t = new Text("Player Name:");
		p_name_t.setFont(new Font(30.0));
		String p_name = settings_loader.getPlayerName();
		p_name_tf = new TextField(p_name);
		p_name_tf.positionCaret(p_name.length());
		p_name_tf.textProperty().addListener((observable, old_name, new_name) -> {
			if (new_name.length() > 15) p_name_tf.deleteNextChar();
			else 
			{
				String new_chars_remaining = chars_remaining_t.getText();
				new_chars_remaining = new_chars_remaining.substring(new_chars_remaining.indexOf(' '));
				chars_remaining_t.setText((15 - new_name.length()) + new_chars_remaining);
			}
		});
		
		select_reso_t = new Text("Select a Game Resolution:");
		select_reso_t.setFont(new Font(30.0));
		resolutions = new ComboBox<String>();
		ArrayList<String> valid_resolutions = settings_loader.getValidResolutions();
		if (valid_resolutions != null)
		{
			resolutions.getItems().addAll(valid_resolutions);
			String selected_reo = settings_loader.getSelectedResolution();
			resolutions.getSelectionModel().select(selected_reo);
		}
		
		prev_menu = new ImageView();
		Image main_menu_img1 = new Image("file:resources/images/onlinegame_menu/main_menu_btn.png");
		Image main_menu_img2 = new Image("file:resources/images/onlinegame_menu/main_menu_btn2.png");
		prev_menu.setFitWidth(200);
		prev_menu.setFitHeight(150);
		prev_menu.setImage(main_menu_img1);
		prev_menu.setOnMouseEntered(event -> changeImage(prev_menu, main_menu_img2));
		prev_menu.setOnMouseExited(event -> changeImage(prev_menu, main_menu_img1));
		prev_menu.setOnMouseClicked(event -> previousMenu());
		
		save_settings = new ImageView();
		Image save_settings_img1 = new Image("file:resources/images/settings_menu/save_settings_btn.png");
		Image save_settings_img2 = new Image("file:resources/images/settings_menu/save_settings_btn2.png");
		save_settings.setFitWidth(200);
		save_settings.setFitHeight(150);
		save_settings.setImage(save_settings_img1);
		save_settings.setOnMouseEntered(event -> changeImage(save_settings, save_settings_img2));
		save_settings.setOnMouseExited(event -> changeImage(save_settings, save_settings_img1));
		save_settings.setOnMouseClicked(event -> saveSettings());
	}

	@Override
	public void run()
	{
		if (resolutions.getItems().isEmpty())
		{
			previousMenu();
			return;
		}
		else saveSettings();
		
		String new_title = stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
		stage.setTitle(new_title + "Game Settings");
		hbox.getChildren().clear();
		hbox1.getChildren().clear();
		
		hbox.getChildren().add(p_name_t);
		hbox.getChildren().add(p_name_tf);
		hbox.getChildren().add(chars_remaining_t);
		
		hbox1.getChildren().add(select_reso_t);
		hbox1.getChildren().add(resolutions);
		
		hbox1 = new HBox(10);
		hbox1.setAlignment(Pos.CENTER);
		hbox1.getChildren().add(prev_menu);
		hbox1.getChildren().add(save_settings);
		parent.getChildren().add(hbox1);
		
		Scene main_menu_scene = parent.getScene();
		if (!stage.getScene().equals(main_menu_scene)) stage.setScene(main_menu_scene);

	}

	@Override
	public void previousMenu()
	{
		saveSettings();
		if (!resolutions.getItems().isEmpty())
		{
			parent.getChildren().remove(hbox1);
			hbox1 = (HBox) parent.getChildren().get(1);
		}
		previous_menu.run();
	}

	@Override
	public void changeImage(ImageView img_space, Image new_img)
	{
		img_space.setImage(new_img);
	}
	
	public void saveSettings()
	{
		String selected_reso = resolutions.getSelectionModel().getSelectedItem();
		if (selected_reso != null && !selected_reso.equals(prev_selected_reso))
		{
			if (!settings_loader.writeScreenResolutionToFile(selected_reso)) file_error_alert.show();
			else prev_selected_reso = selected_reso;
		}
		
		String p_name = p_name_tf.getText();
		if (!p_name.equals(prev_p_name) && p_name.length() > 0)
		{
			if(!settings_loader.writePlayerNameToFile(p_name)) file_error_alert.show();
			else prev_p_name = p_name;
		}
	}
}
