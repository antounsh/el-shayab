package gui;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.server.Server;
import util.SettingsLoader;

public class OnlineGameMenu implements Menu
{
	private VBox parent;
	private HBox hbox, hbox1;
	private ImageView create_server, join_server, prev_menu;
	private Stage stage;
	private Menu previous_menu;
	private boolean server_created;
	
	public OnlineGameMenu(VBox stage_parent, Menu previous_menu)
	{
		parent = stage_parent;
		stage = (Stage) stage_parent.getScene().getWindow();
		this.previous_menu = previous_menu;
		hbox = (HBox) parent.getChildren().get(0);
		hbox1 = (HBox) parent.getChildren().get(1);
		
		create_server = new ImageView();
		Image create_server_img1 = new Image("file:resources/images/onlinegame_menu/create_server_btn.png");
		Image create_server_img2 = new Image("file:resources/images/onlinegame_menu/create_server_btn2.png");
		create_server.setFitWidth(200);
		create_server.setFitHeight(150);
		create_server.setImage(create_server_img1);
		create_server.setOnMouseEntered(event -> changeImage(create_server, create_server_img2));
		create_server.setOnMouseExited(event -> changeImage(create_server, create_server_img1));
		create_server.setOnMouseClicked(event -> displayCreateServerMenu());
		
		join_server = new ImageView();
		Image join_server_img1 = new Image("file:resources/images/onlinegame_menu/join_server_btn.png");
		Image join_server_img2 = new Image("file:resources/images/onlinegame_menu/join_server_btn2.png");
		join_server.setFitWidth(200);
		join_server.setFitHeight(150);
		join_server.setImage(join_server_img1);
		join_server.setOnMouseEntered(event -> changeImage(join_server, join_server_img2));
		join_server.setOnMouseExited(event -> changeImage(join_server, join_server_img1));
		join_server.setOnMouseClicked(event -> displayJoinServerMenu());
		
		prev_menu = new ImageView();
		Image main_menu_img1 = new Image("file:resources/images/onlinegame_menu/main_menu_btn.png");
		Image main_menu_img2 = new Image("file:resources/images/onlinegame_menu/main_menu_btn2.png");
		prev_menu.setFitWidth(200);
		prev_menu.setFitHeight(150);
		prev_menu.setImage(main_menu_img1);
		prev_menu.setOnMouseEntered(event -> changeImage(prev_menu, main_menu_img2));
		prev_menu.setOnMouseExited(event -> changeImage(prev_menu, main_menu_img1));
		prev_menu.setOnMouseClicked(event -> previousMenu());
	}

	@Override
	public void run()
	{
		String new_title = stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
		stage.setTitle(new_title + "Online Game Menu");
		hbox.getChildren().clear();
		hbox1.getChildren().clear();
		server_created = false;
		hbox.getChildren().add(create_server);
		hbox.getChildren().add(join_server);
		hbox1.getChildren().add(prev_menu);
		
		Scene ogm_scene = parent.getScene();
		if (!stage.getScene().equals(ogm_scene)) stage.setScene(ogm_scene);
	}

	@Override
	public void previousMenu()
	{
		previous_menu.run();
	}
	
	@Override
	public void changeImage(ImageView img_space, Image new_img)
	{
		img_space.setImage(new_img);
	}
	
	public String loadPlayerName()
	{
		SettingsLoader settings_loader = new SettingsLoader(GameController.user_config_loc, GameController.game_config_loc);
		return settings_loader.getPlayerName();
	}
	
	private void showErrorAlert()
	{
		Alert file_error_alert = new Alert(AlertType.ERROR);
		file_error_alert.setHeaderText("Invalid player name!");
		file_error_alert.setContentText("Please change your name in the settings.");
		file_error_alert.show();
		previousMenu();
	}
	
	private void displayCreateServerMenu()
	{
		if (!server_created)
		{
			String p_name = loadPlayerName();
			if (p_name.isEmpty())
			{
				showErrorAlert();
				return;
			}
			
			Server server = new Server();
			server.start();
			
			ServerLobby server_lobby = new ServerLobby(parent, this, p_name, "127.0.0.1", server.getPort(), true);
			server_lobby.run();
			server_created = true;
		}
	}

	private void displayJoinServerMenu()
	{
		String p_name = loadPlayerName();
		if (p_name.isEmpty())
		{
			showErrorAlert();
			return;
		}
		
		Menu m = new JoinServerMenu(parent, this, p_name);
		m.run();
	}
}