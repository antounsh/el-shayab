package gui;

import java.io.FileInputStream;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainMenu implements Menu
{
	private VBox menu;
	private HBox hbox, hbox1;
	private ImageView offline_game, online_game, settings, exit_game;
	private Stage stage;
	
	public MainMenu(Stage javafx_stage)
	{
		stage = javafx_stage;
		menu = new VBox(10);
		menu.setAlignment(Pos.CENTER);
		menu.setPrefWidth(650);
		menu.setPrefHeight(400);
		
		hbox = new HBox(10);
		hbox.setAlignment(Pos.CENTER);
		menu.getChildren().add(hbox);
		hbox1 = new HBox(10);
		hbox1.setAlignment(Pos.CENTER);
		menu.getChildren().add(hbox1);
		stage.setScene(new Scene(menu));
		
		offline_game = new ImageView();
		Image offline_game_img1 = new Image("file:resources/images/main_menu/offlinegame_btn.png");
		Image offline_game_img2 = new Image("file:resources/images/main_menu/offlinegame_btn2.png");
		offline_game.setFitWidth(200);
		offline_game.setFitHeight(150);
		offline_game.setImage(offline_game_img1);
		offline_game.setOnMouseEntered(event -> changeImage(offline_game, offline_game_img2));
		offline_game.setOnMouseExited(event -> changeImage(offline_game, offline_game_img1));
		offline_game.setOnMouseClicked(event -> createOfflineGame());
		
		online_game = new ImageView();
		Image online_game_img1 = new Image("file:resources/images/main_menu/onlinegame_btn.png");
		Image online_game_img2 = new Image("file:resources/images/main_menu/onlinegame_btn2.png");
		online_game.setFitWidth(200);
		online_game.setFitHeight(150);
		online_game.setImage(online_game_img1);
		online_game.setOnMouseEntered(event -> changeImage(online_game, online_game_img2));
		online_game.setOnMouseExited(event -> changeImage(online_game, online_game_img1));
		online_game.setOnMouseClicked(event -> displayOnlineGameMenu());
		
		settings = new ImageView();
		Image settings_img1 = new Image("file:resources/images/main_menu/settings_btn.png");
		Image settings_img2 = new Image("file:resources/images/main_menu/settings_btn2.png");
		settings.setFitWidth(200);
		settings.setFitHeight(150);
		settings.setImage(settings_img1);
		settings.setOnMouseEntered(event -> changeImage(settings, settings_img2));
		settings.setOnMouseExited(event -> changeImage(settings, settings_img1));
		settings.setOnMouseClicked(event -> displaySettingsMenu());
		
		exit_game = new ImageView();
		Image exit_game_img1 = new Image("file:resources/images/main_menu/exitgame_btn.png");
		Image exit_game_img2 = new Image("file:resources/images/main_menu/exitgame_btn2.png");
		exit_game.setFitWidth(200);
		exit_game.setFitHeight(150);
		exit_game.setImage(exit_game_img1);
		exit_game.setOnMouseEntered(event -> changeImage(exit_game, exit_game_img2));
		exit_game.setOnMouseExited(event -> changeImage(exit_game, exit_game_img1));
		exit_game.setOnMouseClicked(event -> previousMenu());
	}

	@Override
	public void run()
	{
		String new_title = stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
		stage.setTitle(new_title + "Main Menu");
		hbox.getChildren().clear();
		hbox1.getChildren().clear();
		hbox.getChildren().add(offline_game);
		hbox.getChildren().add(online_game);
		hbox1.getChildren().add(settings);
		hbox1.getChildren().add(exit_game);
		
		Scene main_menu_scene = menu.getScene();
		if (!stage.getScene().equals(main_menu_scene)) stage.setScene(main_menu_scene);
	}

	@Override
	public void previousMenu()
	{
		stage.close();
	}

	@Override
	public void changeImage(ImageView img_space, Image new_img)
	{
		img_space.setImage(new_img);
	}
	
	private void displaySettingsMenu()
	{
		Menu m = new SettingsMenu(menu, this);
		m.run();
	}
	
	private void displayOnlineGameMenu()
	{
		Menu m = new OnlineGameMenu(menu, this);
		m.run();
	}
	
	private void createOfflineGame()
	{
		try
		{
			stage.hide();
			stage.setTitle("El Shayab - Offline Game");
			
			FXMLLoader loader = new FXMLLoader();
			FileInputStream fxmlStream = new FileInputStream("resources/create_offlinegame_menu.fxml");
			loader.setController(new CreateOfflineGameController());
			GridPane create_game = (GridPane) loader.load(fxmlStream);
			Scene scene = new Scene(create_game);
			stage.setScene(scene);
			stage.show();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
