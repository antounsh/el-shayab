package gui;

import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class JoinServerMenu implements Menu
{
	private VBox parent;
	private HBox hbox, hbox1;
	private ImageView connect, prev_menu;
	private Stage stage;
	private Menu previous_menu;
	private TextField server_address;
	private Text server_address_t;
	private boolean is_connecting;
	private String p_name;
	
	public JoinServerMenu(VBox stage_parent, Menu previous_menu, String player_name)
	{
		parent = stage_parent;
		stage = (Stage) stage_parent.getScene().getWindow();
		this.previous_menu = previous_menu;
		hbox = (HBox) parent.getChildren().get(0);
		hbox1 = (HBox) parent.getChildren().get(1);
		is_connecting = false;
		p_name = player_name;
		
		server_address_t = new Text("Server Address:");
		server_address_t.setFont(new Font(30));
		server_address = new TextField();
		
		prev_menu = new ImageView();
		Image previous_menu_img1 = new Image("file:resources/images/join_server_menu/previous_menu_btn.png");
		Image previous_menu_img2 = new Image("file:resources/images/join_server_menu/previous_menu_btn2.png");
		prev_menu.setFitWidth(200);
		prev_menu.setFitHeight(150);
		prev_menu.setImage(previous_menu_img1);
		prev_menu.setOnMouseEntered(event -> changeImage(prev_menu, previous_menu_img2));
		prev_menu.setOnMouseExited(event -> changeImage(prev_menu, previous_menu_img1));
		prev_menu.setOnMouseClicked(event -> previousMenu());
		
		connect = new ImageView();
		Image connect_img1 = new Image("file:resources/images/join_server_menu/connect_btn.png");
		Image connect_img2 = new Image("file:resources/images/join_server_menu/connect_btn2.png");
		connect.setFitWidth(200);
		connect.setFitHeight(150);
		connect.setImage(connect_img1);
		connect.setOnMouseEntered(event -> changeImage(connect, connect_img2));
		connect.setOnMouseExited(event -> changeImage(connect, connect_img1));
		connect.setOnMouseClicked(event -> connectServerLobby());
		
		server_address.setText("127.0.0.1:1234");
	}

	@Override
	public void run()
	{
		String new_title = stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
		stage.setTitle(new_title + "Join Server Menu");
		hbox.getChildren().clear();
		hbox1.getChildren().clear();
		hbox.getChildren().add(server_address_t);
		hbox.getChildren().add(server_address);
		hbox1.getChildren().add(prev_menu);
		hbox1.getChildren().add(connect);
		
		Scene join_server_menu_scene = parent.getScene();
		if (!stage.getScene().equals(join_server_menu_scene)) stage.setScene(join_server_menu_scene);
	}

	@Override
	public void previousMenu()
	{
		previous_menu.run();
	}

	@Override
	public void changeImage(ImageView img_space, Image new_img)
	{
		img_space.setImage(new_img);
	}
	
	private void connectServerLobby()
	{
		if (is_connecting) return;
		
		is_connecting = true;
		String address = server_address.getText();
		int port;
		try
		{
			port = Integer.parseInt(address.substring(address.indexOf(":")+1));
			address = address.substring(0, address.indexOf(":"));
			
			ServerLobby server_lobby = new ServerLobby(parent, this, p_name, address, port, false);
			server_lobby.run();
			is_connecting = false;
		} catch (Exception e)
		{
		}
		System.out.println("THREAD IS : " + Thread.currentThread() + " " + Thread.activeCount());
	}
}