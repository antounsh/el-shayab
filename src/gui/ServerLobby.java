package gui;

import java.util.ArrayList;
import java.util.Iterator;

import game.PlayerType;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Stage;
import net.client.Client;
import net.client.OnlineGameController;
import net.client.lobby.HostCloseRequest;
import net.client.lobby.ClientClosingProcedures;
import net.client.lobby.ClientLeaveRequest;
import net.client.lobby.ClientNameChangeRequest;
import net.client.lobby.ClientReadyRequest;
import net.client.lobby.HostStartGameRequest;
import net.client.lobby.ClientStartingGameProcedures;
import net.server.lobby.ServerStartGameRequestAccepted;
import util.NetworkChecker;
import util.SettingsLoader;

public class ServerLobby implements Menu
{
	private VBox parent;
	private HBox hbox, hbox1;
	private ImageView prev_menu, change_name, ready_btn;
	private Image ready_btn_img2, ready_btn_clicked_img2;
	private Stage stage;
	private ClientClosingProcedures client_closing_procedures;
	private Menu previous_menu;
	private Client client;
	private OnlineGameController game_controller;
	private SettingsLoader settings_loader;
	private String ip_address;
	private int port;
	private int client_pos;
	private boolean is_ready;
	private boolean is_closing;
	private boolean is_host;
	private ArrayList<String> c_names;
	private ArrayList<VBox> p_vbs;
	
	public ServerLobby(VBox stage_parent, Menu previous_menu, String player_name, String ip_address, int port, boolean is_host)
	{
		parent = stage_parent;
		this.previous_menu = previous_menu;
		this.ip_address = ip_address;
		this.port = port;
		this.is_host = is_host;
		game_controller = null;
		is_ready = false;
		is_closing = false;
		stage = (Stage) parent.getScene().getWindow();
		stage.setOnCloseRequest(event -> event.consume());
		settings_loader = new SettingsLoader(GameController.user_config_loc, GameController.game_config_loc);
		
		Text server_info = new Text("Server IP: " + getPublicIPAddressAndPort());
		server_info.setFont(new Font(20));
		hbox = (HBox) parent.getChildren().get(0);
		hbox.getChildren().clear();
		hbox.getChildren().add(server_info);
		
		hbox = (HBox) parent.getChildren().get(1);
		hbox1 = new HBox(10);
		hbox1.setAlignment(Pos.CENTER);
		parent.getChildren().add(hbox1);
		p_vbs = new ArrayList<VBox>();
		
		change_name = new ImageView();
		Image change_name_img1 = new Image("file:resources/images/server_lobby/change_name_btn.png");
		Image change_name_img2 = new Image("file:resources/images/server_lobby/change_name_btn1.png");
		change_name.setFitWidth(200);
		change_name.setFitHeight(150);
		change_name.setImage(change_name_img1);
		change_name.setOnMouseEntered(event -> changeImage(change_name, change_name_img2));
		change_name.setOnMouseExited(event -> changeImage(change_name, change_name_img1));
		change_name.setOnMouseClicked(event -> requestNameChange());
		
		prev_menu = new ImageView();
		Image previous_menu_img1 = new Image("file:resources/images/server_lobby/exit_lobby_btn.png");
		Image previous_menu_img2 = new Image("file:resources/images/server_lobby/exit_lobby_btn1.png");
		prev_menu.setFitWidth(200);
		prev_menu.setFitHeight(150);
		prev_menu.setImage(previous_menu_img1);
		prev_menu.setOnMouseEntered(event -> changeImage(prev_menu, previous_menu_img2));
		prev_menu.setOnMouseExited(event -> changeImage(prev_menu, previous_menu_img1));
		prev_menu.setOnMouseClicked(event -> close(true));
		
		ready_btn = new ImageView();
		Image ready_btn_img1;
		if (!this.is_host)
		{
			ready_btn_img1 = new Image("file:resources/images/server_lobby/ready_btn.png");
			ready_btn_img2 = new Image("file:resources/images/server_lobby/ready_btn1.png");
			Image ready_btn_clicked_img1 = new Image("file:resources/images/server_lobby/ready_btn_clicked.png");
			ready_btn_clicked_img2 = new Image("file:resources/images/server_lobby/ready_btn_clicked1.png");
			ready_btn.setOnMouseEntered(event -> {
				if(is_ready) changeImage(ready_btn, ready_btn_clicked_img2);
				else changeImage(ready_btn, ready_btn_img2);
			});
			ready_btn.setOnMouseExited(event -> {
				if(is_ready) changeImage(ready_btn, ready_btn_clicked_img1);
				else changeImage(ready_btn, ready_btn_img1);
			});
		}
		else
		{
			ready_btn_img1 = new Image("file:resources/images/create_server_menu/start_game_btn.png");
			ready_btn_img2 = new Image("file:resources/images/create_server_menu/start_game_btn1.png");
			ready_btn.setOnMouseEntered(event -> changeImage(ready_btn, ready_btn_img2));
			ready_btn.setOnMouseExited(event -> changeImage(ready_btn, ready_btn_img1));
		}
		ready_btn.setOnMouseClicked(event -> readyRequest());
		ready_btn.setImage(ready_btn_img1);
		ready_btn.setFitWidth(200);
		ready_btn.setFitHeight(150);
		
		createPlayerVBoxes();
        client_closing_procedures = new ClientClosingProcedures(stage);
        client = new Client(port, ip_address, player_name, this);
		if (!isConnected())
		{
			previousMenu();
			return;
		}
        client.start();
	}
	
	@Override
	public void previousMenu()
	{
		stage.setOnCloseRequest(event -> stage.close());
		Iterator<Node> iter = parent.getChildren().iterator();
		while (iter.hasNext()) 
		{
			HBox curr_hb = (HBox) iter.next();
			curr_hb.getChildren().clear();
		}
		previous_menu.run();
	}

	@Override
	public void changeImage(ImageView img_space, Image new_img)
	{
		img_space.setImage(new_img);
	}
	
	@Override
	public void run()
	{
		String new_title = stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
		stage.setTitle(new_title + "Server Lobby");
		stage.setOnCloseRequest(event -> close(false));
		hbox.getChildren().clear();
		hbox1.getChildren().clear();
		
		int size = p_vbs.size();
		for (int i = 0; i < size; i++)
		{
			VBox curr_vb = p_vbs.get(i);
			hbox.getChildren().add(curr_vb);
		}
		hbox1.getChildren().add(prev_menu);
		hbox1.getChildren().add(change_name);
		hbox1.getChildren().add(ready_btn);
		
		Scene lobby_scene = parent.getScene();
		if (!stage.getScene().equals(lobby_scene))
		{
			stage.setScene(lobby_scene);
			stage.show();
		}
	}
	
	public void updateServerIP(String new_ip_address, int new_port)
	{
		port = new_port;
		ip_address = new_ip_address;
		
		HBox temp_hbox = (HBox) parent.getChildren().get(0);
		Text server_info = (Text) temp_hbox.getChildren().get(0);
		String server_info_text = server_info.getText();
		server_info_text = server_info_text.substring(0, (server_info_text.indexOf(':') + 1)) + " " + getPublicIPAddressAndPort();
		server_info.setText(server_info_text);
	}
	
	public void setShowPreviousMenu(boolean show_previous_menu)
	{
		if (show_previous_menu) client_closing_procedures.setServerLobby(this);
		else client_closing_procedures.setServerLobby(null);
	}
	
	public void hideAlert()
	{
		client_closing_procedures.setPreClose(false);
		Platform.runLater(client_closing_procedures);
	}
	
	public void showAlert(boolean show_close_alert)
	{
		client_closing_procedures.setShowCloseAlert(show_close_alert);
		client_closing_procedures.setPreClose(true);
		Platform.runLater(client_closing_procedures);
	}
	
	public void close(boolean show_stage)
	{
		if(!is_closing)
		{
			is_closing = true;
			setShowPreviousMenu(show_stage);
			
			if (is_host) client.send(new HostCloseRequest(show_stage));
			else
			{
				client.send(new ClientLeaveRequest());
				hideAlert();
				showAlert(true);
			}
		}
	}
	
	public void previousPreviousMenu()
	{
		previousMenu();
		if (!is_host) previous_menu.previousMenu();
	}
	
	public void setClientPos(int client_pos)
	{
		this.client_pos = client_pos;
	}
	
	public void setReady(boolean is_ready)
	{
		if (!is_host)
		{
			this.is_ready = is_ready;
			if(this.is_ready) changeImage(ready_btn, ready_btn_clicked_img2);
			else changeImage(ready_btn, ready_btn_img2);
		}
	}
	
	public void setClientNames(ArrayList<String> client_names)
	{
		c_names = client_names;
		int size = client_names.size();
		for (int i = 0; i < size; i++)
		{
			VBox curr_vb = p_vbs.get(i);
			ObservableList<Node> nodes = curr_vb.getChildren();
			
			Text p_n_t = (Text) nodes.get(0);
			Text p_n = (Text) nodes.get(1);
			String p_name = c_names.get(i);
			p_n.setText(p_name);
			p_n.setVisible(true);
			
			boolean is_client_pos = (i == client_pos);
			if (is_client_pos) settings_loader.writePlayerNameToFile(p_name);
			p_n_t.setVisible(is_client_pos);
			TextField tf = (TextField) nodes.get(2);
			tf.setVisible(is_client_pos);
			Text chars_remaining = (Text) nodes.get(3);
			chars_remaining.setVisible(is_client_pos);
		}
		
		size = p_vbs.size();
		for (int i = client_names.size(); i < size; i++)
		{
			VBox curr_vb = p_vbs.get(i);
			ObservableList<Node> nodes = curr_vb.getChildren();
			for (Node curr_node : nodes) curr_node.setVisible(false);
		}
	}
	
	public OnlineGameController startGame(ServerStartGameRequestAccepted data)
	{
		ArrayList<String> p_names = new ArrayList<String>();
		ArrayList<PlayerType> players = data.getPlayers();
		for (PlayerType player : players) p_names.add(player.getPlayerName());
		
		ClientStartingGameProcedures game_proc = new ClientStartingGameProcedures(stage, p_names, players, client, data);
		Platform.runLater(game_proc);
		while(!game_proc.isDone());
		
		game_controller = game_proc.getGameController();
		return game_controller;
	}
	
	public String getPublicIPAddressAndPort()
	{
		String public_server_ip = ip_address;
		if (is_host)
		{
			try
			{
				public_server_ip = NetworkChecker.getIP();
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return public_server_ip + ":" + port;
	}
	
	public boolean isHost()
	{
		return is_host;
	}
	
	public boolean isConnected()
	{
		return client.isConnected();
	}
	
	private void requestNameChange()
	{
		VBox curr_vb = p_vbs.get(client_pos);
		TextField tf = (TextField) curr_vb.getChildren().get(2);
		String tf_text = tf.getText();
		if (!tf_text.isEmpty()) client.send(new ClientNameChangeRequest(tf_text));
	}
	
	private void readyRequest()
	{
		if (!is_host && isConnected()) client.send(new ClientReadyRequest());
		else if (is_host && isConnected()) client.send(new HostStartGameRequest());
	}
	
	private void createPlayerVBoxes()
	{
		for (int i = 0; i < 4; i++)
		{
			VBox curr_p = new VBox();
			curr_p.setAlignment(Pos.CENTER);
			Text curr_p_t = new Text("***HOST***");
			curr_p_t.setFont(new Font(20));
			curr_p_t.setFill(Color.RED);
			if (this.is_host) curr_p_t.setVisible(true);
			else curr_p_t.setVisible(false);
			curr_p.getChildren().add(curr_p_t);
			
			Text curr_p_n = new Text("");
			curr_p_n.setTextAlignment(TextAlignment.CENTER);
			curr_p_n.setFont(new Font(20));
			curr_p_n.setWrappingWidth(142);
			curr_p.getChildren().add(curr_p_n);
			
			Text chars_remaining_t = new Text("15 Characters Remaining");
			chars_remaining_t.setFont(new Font(12.0));
			chars_remaining_t.setVisible(false);
			TextField p_name_tf = new TextField();
			p_name_tf.setVisible(false);
			p_name_tf.textProperty().addListener((observable, old_name, new_name) -> {
				if (new_name.length() > 15) p_name_tf.deleteNextChar();
				else 
				{
					String new_chars_remaining = chars_remaining_t.getText();
					new_chars_remaining = new_chars_remaining.substring(new_chars_remaining.indexOf(' '));
					chars_remaining_t.setText((15 - new_name.length()) + new_chars_remaining);
				}
			});
			curr_p.getChildren().add(p_name_tf);
			curr_p.getChildren().add(chars_remaining_t);
			p_vbs.add(curr_p);
		}
	}
}
