package gui;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CreateOfflineGameController
{
	@FXML
	private ComboBox<Integer> numOfPlayers;
	@FXML
	private Text p3_t, p4_t;
	@FXML
	private TextField p1_tf, p2_tf, p3_tf, p4_tf;
	@FXML
	private GridPane gp;
	private ArrayList<String> p_names;
	
	public CreateOfflineGameController()
	{
	}
	
	@FXML
	private void initialize() 
	{
		numOfPlayers.setItems(FXCollections.observableArrayList(2, 3, 4));
		numOfPlayers.getSelectionModel().selectFirst();
		p_names = new ArrayList<String>();
	}
	
	public void test()
	{
		numOfPlayers.getSelectionModel().selectLast();
		p3_t.setVisible(true);
		p3_tf.setVisible(true);
		p4_t.setVisible(true);
		p4_tf.setVisible(true);
		
		p1_tf.setText("Claire");
		p2_tf.setText("Josh");
		p3_tf.setText("Antoun");
		p4_tf.setText("Mina");
		Button btn = (Button) gp.getChildren().get(11);
		btn.fire();
	}
	
	@FXML
	private void updateNumPlayers()
	{
		int selected_item = numOfPlayers.getSelectionModel().getSelectedItem();
		if (selected_item == 2)
		{
			p3_t.setVisible(false);
			p3_tf.setVisible(false);
			p4_t.setVisible(false);
			p4_tf.setVisible(false);
		}
		else if (selected_item == 3)
		{
			p3_t.setVisible(true);
			p3_tf.setVisible(true);
			p4_t.setVisible(false);
			p4_tf.setVisible(false);
		}
		else
		{
			p3_t.setVisible(true);
			p3_tf.setVisible(true);
			p4_t.setVisible(true);
			p4_tf.setVisible(true);
		}
	}
	
	@FXML
	private void buttonPressed()
	{
		int selected_item = numOfPlayers.getSelectionModel().getSelectedItem();
		String p1_n = p1_tf.getText();
		String p2_n = p2_tf.getText();
		String p3_n, p4_n;
		boolean unique_player_names = true;
		
		if (selected_item == 2)
		{
			if (!p1_n.isEmpty() && !p2_n.isEmpty())
			{
				unique_player_names = (p1_n.compareTo(p2_n) != 0);
				if (unique_player_names)
				{
					p_names.add(p1_n);
					p_names.add(p2_n);
					startGame();
					return;
				}
			}
		}
		else if (selected_item == 3)
		{
			p3_n = p3_tf.getText();
			if (!p1_n.isEmpty() && !p2_n.isEmpty() && !p3_n.isEmpty())
			{
				unique_player_names = ((p1_n.compareTo(p2_n) != 0) && (p1_n.compareTo(p3_n) != 0) && (p2_n.compareTo(p3_n) != 0));
				if (unique_player_names)
				{
					p_names.add(p1_n);
					p_names.add(p2_n);
					p_names.add(p3_n);
					startGame();
					return;
				}
			}
		}
		else
		{
			p3_n = p3_tf.getText();
			p4_n = p4_tf.getText();
			if (!p1_n.isEmpty() && !p2_n.isEmpty() && !p3_n.isEmpty() && !p4_n.isEmpty())
			{
				unique_player_names = ((p1_n.compareTo(p2_n) != 0) && (p1_n.compareTo(p3_n) != 0) && (p1_n.compareTo(p4_n) != 0) && (p2_n.compareTo(p3_n) != 0) && (p2_n.compareTo(p4_n) != 0) && (p3_n.compareTo(p4_n) != 0));
				if (unique_player_names)
				{
					p_names.add(p1_n);
					p_names.add(p2_n);
					p_names.add(p3_n);
					p_names.add(p4_n);
					startGame();
					return;
				}
			}
		}
		missingFieldsAlert(unique_player_names);
	}
	
	public ArrayList<String> getPlayerNames()
	{
		return p_names;
	}
	
	private void startGame()
	{
		Stage stage = (Stage) gp.getScene().getWindow();
		stage.hide();
		GameController game_controller = new GameController(getPlayerNames(), false, stage);
		game_controller.loadSettings(game_controller);
		stage.show();
	}
	
    private void missingFieldsAlert(boolean unique_player_names) 
    {
    	Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("El Shayab - Error");
        alert.setHeaderText(null);
        String error_msg = "Please fill out the player name(s).";
        
        Stage main_stage = (Stage) gp.getScene().getWindow();
        Stage alert_stage = (Stage) alert.getDialogPane().getScene().getWindow();
        alert_stage.getIcons().add(main_stage.getIcons().get(0));
        
    	if (!unique_player_names)
    		error_msg = "Please make sure each player name is unique.";
        
        alert.setContentText(error_msg);
        alert.showAndWait();
    }

}