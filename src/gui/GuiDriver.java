package gui;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class GuiDriver extends Application
{
	public static void main(String[] args) 
	{
		launch(args);
	}
	
	@Override
	public void start(Stage stage)
	{
		try
		{
			String icon_loc = "file:resources/images/icon.png";
			stage.getIcons().add(new Image(icon_loc));
			stage.setTitle("El Shayab - ");
			stage.setResizable(true);
			stage.sizeToScene();
			stage.centerOnScreen();
			MainMenu main_menu = new MainMenu(stage);
			main_menu.run();
			stage.show();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}