package gui;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import game.*;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.*;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import util.SettingsLoader;

public class GameController
{
	public static String user_config_loc = "resources/user_config.ini", game_config_loc = "resources/game_config.ini";
	
	@FXML
	protected Text total_mcs;
	@FXML
	protected GridPane gp;
	@FXML
	protected ImageView options_img_space;
	@FXML
	protected Button end_turn_btn;
	
	protected Stage stage;
	protected ArrayList<Card> deck;
	protected ArrayList<String> p_names;
	protected int[] p_indexes;
	
	private Players players;
	private Image options_img1, options_img2;
	private boolean online_game, curr_player_disabled;
	
	public GameController(ArrayList<String> player_names, boolean online_game, Stage main_stage)
	{
		stage = main_stage;
		stage.setResizable(false);
		p_names = player_names;
		this.online_game = online_game;
		options_img1 = new Image("file:resources/images/options.png");
		options_img2 = new Image("file:resources/images/options2.png");
	}
	
	@FXML
	public void initialize() 
	{
		curr_player_disabled = true;
		initialSetup();
		gp.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ESCAPE) displayOptionsScreen();
		});
		
		for (int i = 0; i < 4; i++)
		{
			VBox vb = (VBox) gp.getChildren().get(i);
			FlowPane fp = (FlowPane) vb.getChildren().get(1);
			Iterator<Node> iter = fp.getChildren().iterator();
			int img_pos = 0;
			while(iter.hasNext())
			{
				Node item = iter.next();
				boolean is_img = item instanceof ImageView;
				int vb_pos = i;
				if (!is_img)
				{
					HBox hb = (HBox) item;
					Button prev = (Button) hb.getChildren().get(0);
					Button next = (Button) hb.getChildren().get(1);
					
					prev.setOnAction(event -> playerPrevNextPressed(vb_pos, false));
					next.setOnAction(event -> playerPrevNextPressed(vb_pos, true));
				}
				else
				{
					ImageView iv = (ImageView) item;
					iv.setOnMouseEntered(event -> iv.setTranslateY(-25.0));
					iv.setOnMouseExited(event -> iv.setTranslateY(0.0));
					int temp1 = img_pos;
					iv.setOnMouseClicked(event -> cardImageClicked(vb_pos, temp1));
					img_pos++;
				}
			}
		}
		curr_player_disabled = false;
	}
	
	public void loadSettings(GameController game_controller)
	{
		SettingsLoader settings_loader = new SettingsLoader(user_config_loc, game_config_loc);
		try
		{
			String fxml_file_name = settings_loader.getFXMLFileName();
			if (fxml_file_name.isEmpty()) throw new Exception("Selected resolution not supported.");
			
			FXMLLoader loader = new FXMLLoader();
			loader.setController(game_controller);
			FileInputStream fxmlStream = new FileInputStream(fxml_file_name);
			stage.setScene(new Scene(loader.load(fxmlStream)));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void initialSetup()
	{
		int size = p_names.size();
		p_indexes = new int[size];
		
		if (!online_game)
		{
			players = new Players();
			for (int i = 0; i < size; i++)
			{
					Player p = new Player(p_names.get(i));
					players.addPlayer(p);
			}
			
			players.shufflePlayers();
			Deck d = new Deck();
			deck = d.getShuffledDeck();
			passOutCards();
			players.marryAllCards();
		}
		setStageTitle();
		displayBoard();
	}
	
	public void endCurrPlayerTurn()
	{
		if (curr_player_disabled)
		{
			players.nextPlayer();
			players.marryAllCards();
			players.shuffleAllHands();
			displayBoard();
			curr_player_disabled = false;
		}
	}
	
    public void displayOptionsScreen() 
    {
    	setupOptionsScreen(300.0, 150.0);
    	stage.show();
    }
    
	public void changeOptionsImage()
	{
		if (options_img_space.getImage().getUrl().equals(options_img1.getUrl())) options_img_space.setImage(options_img2);
		else options_img_space.setImage(options_img1);
	}
	
	protected void exitGame()
	{
		stage.close();
	}
	
	protected void setStageTitle()
	{
		stage.setOnCloseRequest(event -> exitGame());
		String new_title = stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
		
		if (online_game) new_title += "Online Game";
		else new_title += "Offline Game";
		stage.setTitle(new_title);
	}
	
	protected void displayBoard()
	{
		int num_players = players.numOfPlayers();
		ObservableList<Node> vbs = gp.getChildren();
		
		for (int i = 0; i < num_players; i++)
		{
			VBox curr_vb = (VBox) vbs.get(i);
			ObservableList<Node> nodes = curr_vb.getChildren();
			for (Node curr_node : nodes) curr_node.setVisible(true);
			
			HBox curr_hb = (HBox) curr_vb.getChildren().get(0);
			Text curr_name = (Text) curr_hb.getChildren().get(0);
			
			String curr_player_name = players.getPlayer(i).getPlayerName();
			curr_name.setText(curr_player_name);
			
			boolean show_front = (i == players.getCurrentPlayerPosition());
			displayPlayerCards(i, p_indexes[i], show_front, false);
		}
		
		for (int i = num_players; i < 4; i++)
		{
			VBox curr_vb = (VBox) vbs.get(i);
			ObservableList<Node> nodes = curr_vb.getChildren();
			for (Node curr_node : nodes) curr_node.setVisible(false);
		}
		
		total_mcs.setText(Integer.toString(players.numberOfMarriedCards()));
		highlightPlayer(players.getCurrentPlayerPosition());
	}
	
	protected void unhighlightPlayer(int player_pos)
	{
		VBox p_vb = (VBox) gp.getChildren().get(player_pos);
		HBox p_hb = (HBox) p_vb.getChildren().get(0);
		Text p_name = (Text) p_hb.getChildren().get(0);
		
		String new_name = players.getPlayer(player_pos).getPlayerName();
		p_name.setFill(Color.BLACK);
		p_name.setText(new_name);
	}
	
	protected void highlightPlayer(int player_pos)
	{
		VBox p_vb = (VBox) gp.getChildren().get(player_pos);
		HBox p_hb = (HBox) p_vb.getChildren().get(0);
		Text p_name = (Text) p_hb.getChildren().get(0);
		
		String new_name = "*** " + p_name.getText() + " ***";
		p_name.setText(new_name);
		p_name.setFill(Color.RED);
		
		player_pos--;
		if (player_pos < 0)
			player_pos = players.numOfPlayers() - 1;
		unhighlightPlayer(player_pos);
	}
	
	protected void playerPrevNextPressed(int player_pos, boolean next_pressed)
	{
		if (next_pressed) p_indexes[player_pos] += 9;
		else p_indexes[player_pos] -= 9;
		
		int curr_player_pos = players.getCurrentPlayerPosition();
		boolean show_front = false;
		if (curr_player_pos == player_pos)show_front = true;
		
		displayPlayerCards(player_pos, p_indexes[player_pos], show_front, false);
	}
	
	protected void cardImageClicked(int player_pos, int img_pos)
	{
		int curr_player_pos = players.getCurrentPlayerPosition();
		int next_player_pos = curr_player_pos + 1;
		if (next_player_pos == players.numOfPlayers()) next_player_pos = 0;
		
		if ((next_player_pos == player_pos) && !curr_player_disabled)
		{
			curr_player_disabled = true;
			moveCardToCurrPlayer(next_player_pos, img_pos);
		}
	}
	
	protected void displayPlayerCards(int player_pos, int hand_pos, boolean show_front, boolean player_done)
	{
		VBox vb = (VBox) gp.getChildren().get(player_pos);
		FlowPane fp = (FlowPane) vb.getChildren().get(1);
		
		Iterator<Node> iter = fp.getChildren().iterator();
		ArrayList<ImageView> imgs = new ArrayList<ImageView>();
		Button prev = null, next = null;
		while (iter.hasNext())
		{
			Node item = iter.next();
			boolean is_img = item instanceof ImageView;
			if (is_img)
			{
				ImageView img = (ImageView) item;
				img.setVisible(false);
				imgs.add(img);
			}
			else
			{
				HBox hb = (HBox) item;
				prev = (Button) hb.getChildren().get(0);
				next = (Button) hb.getChildren().get(1);
			}
		}
		
		if (player_done)
		{
			next.setVisible(false);
			prev.setVisible(false);
			return;
		}
		
		int p_hand_size = displayPlayerCardsHelper(player_pos, hand_pos, show_front, imgs);
		
		if ((p_indexes[player_pos] + 9) < p_hand_size) next.setVisible(true);
		else next.setVisible(false);
		
		if ((p_indexes[player_pos] - 9) < 0) prev.setVisible(false);
		else prev.setVisible(true);
	}
	
	protected VBox setupOptionsScreen(double scene_width, double scene_height)
	{
		Scene main_scene = gp.getScene();
    	stage.hide();
    	VBox options_menu = new VBox();
    	options_menu.setAlignment(Pos.CENTER);
    	options_menu.setSpacing(10.0);
    	
    	Scene new_scene = new Scene(options_menu, scene_width, scene_height);
    	stage.setScene(new_scene);
    	stage.setOnCloseRequest(event -> {
    		event.consume();
    		stage.hide();
			setStageTitle();
			stage.setScene(main_scene);
			stage.show();
    	});
    	
    	Button btn = new Button("New Game");
		btn.setDefaultButton(true);
		btn.setCancelButton(false);
		btn.setTextAlignment(TextAlignment.CENTER);
		btn.setOnAction(event -> {
			stage.hide();
			setStageTitle();
			stage.setScene(main_scene);
			changeStage(false, false);
		});
    	options_menu.getChildren().add(btn);
    	
    	btn = new Button("Restart Game");
		btn.setDefaultButton(false);
		btn.setCancelButton(false);
		btn.setTextAlignment(TextAlignment.CENTER);
		btn.setOnAction(event -> {
			stage.hide();
			setStageTitle();
			stage.setScene(main_scene);
			changeStage(false, true);
		});
    	options_menu.getChildren().add(btn);
    	
    	btn = new Button("Exit Game");
		btn.setDefaultButton(false);
		btn.setCancelButton(false);
		btn.setTextAlignment(TextAlignment.CENTER);
		btn.setOnAction(event -> exitGame());
    	options_menu.getChildren().add(btn);
    	
    	btn = new Button("Return to Game");
		btn.setDefaultButton(false);
		btn.setCancelButton(true);
		btn.setTextAlignment(TextAlignment.CENTER);
		btn.setOnAction(event -> {
			stage.hide();
			setStageTitle();
			stage.setScene(main_scene);
			stage.show();
		});
    	options_menu.getChildren().add(btn);
    	
    	return options_menu;
	}
	
	protected HBox setupGameFinishedAlert(String winners_names)
	{
		GridPane game_finished_alert = new GridPane();
    	game_finished_alert.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
    	game_finished_alert.setHgap(10.0);
    	game_finished_alert.setVgap(10.0);
    	
    	stage.hide();
    	double width = 630.0, height = 320.0;
    	stage.setScene(new Scene(game_finished_alert, width, height));
    	String new_title = stage.getTitle();
		new_title = new_title.substring(0, new_title.indexOf("- ") + 2);
		stage.setTitle(new_title + "Game Over!!!");
		
		Image img = new Image("file:resources/images/winner.png");
        ImageView iv = new ImageView(img);
        iv.setFitWidth(250);
        iv.setFitHeight(250);
        game_finished_alert.add(iv, 0, 0);
        
        VBox text_organizer = new VBox();
        text_organizer.setAlignment(Pos.TOP_CENTER);
        
        for (int i = 0; i < 4; i++)
        {
    		Text t = new Text("");
        	t.setStyle("-fx-fill: red; -fx-font-size: 50px; -fx-text-alignment: center; -fx-line-spacing: 0;");
    		if (!winners_names.isEmpty())
    		{
    			int index_of = winners_names.indexOf("\n");
    			if (index_of == -1) 
    			{
    				t.setText(winners_names.substring(0));
    				winners_names = "";
    			}
    			else
    			{
    				t.setText(winners_names.substring(0, index_of));
    				winners_names = winners_names.substring(index_of + 1);
    			}
    		}
        	text_organizer.getChildren().add(t);
        }
        
        HBox buttons_organizer = new HBox();
        buttons_organizer.setAlignment(Pos.BOTTOM_RIGHT);
        
        Button restart = new Button("Restart");
        restart.setDefaultButton(true);
        restart.setCancelButton(false);
        restart.setOnAction(event -> {
        	stage.hide();
			setStageTitle();
			stage.setScene(gp.getScene());
        	changeStage(true, true);
        });
        buttons_organizer.getChildren().add(restart);
        
        Button exit = new Button("Exit Game");
        exit.setDefaultButton(false);
        exit.setCancelButton(true);
        exit.setOnAction(event -> exitGame());
        buttons_organizer.getChildren().add(exit);
        
        game_finished_alert.add(text_organizer, 1, 0);
        game_finished_alert.add(buttons_organizer, 1, 0);
        ColumnConstraints col = new ColumnConstraints();
        col.setHalignment(HPos.LEFT);
        game_finished_alert.getColumnConstraints().add(col);
        
        RowConstraints row = new RowConstraints();
       	row.setValignment(VPos.TOP);
       	game_finished_alert.getRowConstraints().add(row);
        
        col = new ColumnConstraints();
        col.setHalignment(HPos.CENTER);
        col.setPrefWidth(width);
        game_finished_alert.getColumnConstraints().add(col);
        
       	row = new RowConstraints();
       	row.setValignment(VPos.TOP);
       	game_finished_alert.getRowConstraints().add(row);
       	
       	return buttons_organizer;
	}
	
	protected int displayPlayerCardsHelper(int player_pos, int hand_pos, boolean show_front, ArrayList<ImageView> imgs)
	{
		ArrayList<Card> p_hand = players.getPlayer(player_pos).getHand();
		int p_hand_size = p_hand.size();
		int num_displayed_imgs = 0;
		for (int i = hand_pos; i < p_hand_size; i++)
		{
			if (num_displayed_imgs == 9) break;
			
			ImageView img = imgs.get(num_displayed_imgs);
			Card c = p_hand.get(i);
			if (show_front) img.setImage(c.getFrontImage());
			else img.setImage(c.getBackImage());
			
			img.setVisible(true);
			num_displayed_imgs++;
		}
		
		return p_hand_size;
	}
	
	private void passOutCards()
	{
		int num_players = players.numOfPlayers();
		int i = 0;
		while(!deck.isEmpty())
		{
			if (i == num_players) i = 0;
			
			Player p = players.getPlayer(i);
			p.addCard(deck.remove(0));
			i++;
		}
	}
	
    private void gameFinishedAlert() 
    {
        ArrayList<Player> orig_players = players.getOrignalPlayers();
        String winners = "";
        for (Player curr_player : orig_players)
        {
        	if (winners.isEmpty()) winners = curr_player.getPlayerName();
        	else winners += "\n" + curr_player.getPlayerName();
        }
        
        setupGameFinishedAlert(winners);
        stage.show();
    }
	
	private void changeStage(boolean game_finished, boolean restart_game)
	{
		try
		{
			stage.hide();
			
			if ((game_finished && restart_game) || (!game_finished && restart_game)) // restart game
			{
				curr_player_disabled = true;
				p_names = new ArrayList<String>();
				Iterator<Player> orig_players_iter = players.getOrignalPlayers().iterator();
				while(orig_players_iter.hasNext())
					p_names.add(orig_players_iter.next().getPlayerName());
				
				initialSetup();
				curr_player_disabled = false;
				stage.show();
			}
			else if (game_finished && !restart_game) gameFinishedAlert(); // game over screen
			else // new game
			{
				FXMLLoader loader = new FXMLLoader();
				loader.setController(new CreateOfflineGameController());
				FileInputStream fxmlStream = new FileInputStream("resources/create_offlinegame_menu.fxml");
				
				GridPane screen2 = (GridPane) loader.load(fxmlStream);
				Scene scene = new Scene(screen2);
				
				stage.setScene(scene);
				stage.show();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void playerFinished(int player_pos)
	{
		players.removePlayer(player_pos);
		int new_player_size = p_indexes.length - 1;
		if (new_player_size == 1)
		{
			changeStage(true, false);
			return;
		}
		
		int[] new_indexes = new int[new_player_size];
		int index = 0;
		
		for (int i = 0; i < (new_player_size + 1); i++)
		{
			if (i == player_pos) continue;
			
			new_indexes[index] = p_indexes[i];
			index++;
		}
		p_indexes = new_indexes;
		players.prevPlayer();
	}
	
	// Adds Next Player's card to Curr Player
	private void moveCardToCurrPlayer(int next_player_pos, int clicked_img_pos)
	{
		int curr_player_pos = players.getCurrentPlayerPosition();
		Player curr_player = players.getPlayer(curr_player_pos);
		Player next_player = players.getPlayer(next_player_pos);
		
		int card_loc = p_indexes[next_player_pos] + clicked_img_pos;
		curr_player.addCard(next_player.removeCard(card_loc));
		curr_player.marryCards();
		boolean curr_player_done = (curr_player.numberOfCards() == 0);
		boolean next_player_done = (next_player.numberOfCards() == 0);
		
		if (!curr_player_done)
		{
			p_indexes[curr_player_pos] = 0;
			displayPlayerCards(curr_player_pos, 0, true, false);	
		}
		else displayPlayerCards(curr_player_pos, 0, true, true);
		
		if (!next_player_done)
		{
			p_indexes[next_player_pos] = 0;
			displayPlayerCards(next_player_pos, 0, false, false);
		}
		else displayPlayerCards(next_player_pos, 0, false, true);
		
		if (curr_player_done)
		{
			playerFinished(curr_player_pos);
			next_player_pos--;
			if (next_player_pos < 0)
				next_player_pos = 0;
		}
		
		if (next_player_done) playerFinished(next_player_pos);
		total_mcs.setText(Integer.toString(players.numberOfMarriedCards()));
	}
}