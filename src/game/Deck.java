package game;

import java.util.ArrayList;

import javafx.scene.image.Image;
import util.Random;

public class Deck
{
	private ArrayList<Card> unshuffled_deck;
	
	public Deck()
	{
		unshuffled_deck = new ArrayList<Card>();
		createDeck();
		removeThreeKings();
	}
	
	//used for debugging to return the deck before shuffling it
	public ArrayList<Card> getDeck()
	{
		return unshuffled_deck;
	}
	
	public ArrayList<Card> getShuffledDeck()
	{
		Random number_generator = Random.getRandomNumberGenerator();
		ArrayList<Card> shuffled_deck = new ArrayList<Card>();
		@SuppressWarnings("unchecked")
		ArrayList<Card> temp_deck = (ArrayList<Card>) unshuffled_deck.clone();
		int size = temp_deck.size();
		
		for (int i = 0; i < size; i++)
		{
			int random_number = number_generator.randomInt(0, (temp_deck.size() - 1));
			Card curr_card = temp_deck.remove(random_number);
			shuffled_deck.add(curr_card);
		}
		
		return shuffled_deck;
	}
	
	// 1-10 11=J 12=Q 13=K
	// S=Spades H=Hearts C=Clubs D=Diamonds
	private void createDeck()
	{
		int suit_counter = 0;
		char[] suits = {'S', 'H', 'C', 'D'};
		String back_img_loc = "file:resources/images/cards/back.png";
		
		for (int i = 1; i <= 52; i++)
		{
			String front_img_loc = "file:resources/images/cards/" + Integer.toString(i);
			front_img_loc += Character.toLowerCase(suits[suit_counter]) + ".png";
			
			Card card = new Card(i, suits[suit_counter]);
			Image img = new Image(front_img_loc);
			card.setFrontImage(img);
			img = new Image(back_img_loc);
			card.setBackmage(img);
			unshuffled_deck.add(card);
			
			if (suit_counter == 3 && i == 13) break;
			else if (i == 13)
			{
				i = 0;
				suit_counter++;
			}
		}
	}
	
	private void removeThreeKings()
	{
		// 26=Hearts, 39=Clubs, 52=Diamonds
		unshuffled_deck.remove(51);
		unshuffled_deck.remove(38);
		unshuffled_deck.remove(25);
	}
}
