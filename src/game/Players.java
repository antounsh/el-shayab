package game;

import java.util.ArrayList;
import util.Random;

public class Players
{
	private ArrayList<Player> players;
	private ArrayList<Player> orig_players;
	private int current_player_position;
	
	public Players()
	{
		players = new ArrayList<Player>();
		current_player_position = 0;
		orig_players = new ArrayList<Player>();
	}
	
	public void addPlayer(Player player)
	{
		players.add(player);
		orig_players.add(player);
	}
	
	public void shufflePlayers()
	{
		Random number_generator = Random.getRandomNumberGenerator();
		ArrayList<Player> shuffled_players = new ArrayList<Player>();
		int size = players.size();
		
		for (int i = 0; i < size; i++)
		{
			int random_number = number_generator.randomInt(0, (players.size() - 1));
			Player curr_player = players.remove(random_number);
			shuffled_players.add(curr_player);
		}

		players = shuffled_players;
	}
	
	public Player getPlayer(int player_position)
	{
		return players.get(player_position);
	}
	
	public void removePlayer(int player_pos)
	{
		players.remove(player_pos);
	}
	
	public int removePlayerCompletely(Player player)
	{
		int removed_player_pos = players.indexOf(player);
		players.remove(player);
		orig_players.remove(player);
		
		return removed_player_pos;
	}
	
	public int numberOfCards()
	{
		int number_of_cards = 0;
		int size = players.size();
		for (int i = 0; i < size; i++)
		{
			Player curr_player = players.get(i);
			number_of_cards += curr_player.numberOfCards();
		}
		return number_of_cards;
	}
	
	public int numOfPlayers()
	{
		return players.size();
	}
	
	public void prevPlayer()
	{
		current_player_position--;
		if (current_player_position < 0)
			current_player_position = (numOfPlayers() - 1);
	}
	
	public void nextPlayer()
	{
		current_player_position++;
		int numOfPlayers = (players.size() - 1);
		if (current_player_position > numOfPlayers)
			current_player_position = 0;
	}
	
	public void shuffleAllHands()
	{
		int size = players.size();
		
		for (int i = 0; i < size; i++)
		{
			Player curr_player = players.get(i);
			curr_player.shuffleHand();
		}
	}
	
	public void marryAllCards()
	{
		int size = players.size();
		
		for (int i = 0; i < size; i++)
		{
			Player curr_player = players.get(i);
			curr_player.marryCards();
		}
	}
	
	public ArrayList<Player> getOrignalPlayers()
	{
		return orig_players;
	}
	
	public void shuffleCurrentPlayerHand()
	{
		Player curr_player = players.get(current_player_position);
		curr_player.shuffleHand();
	}
	
	public void marryCurrentPlayerCards()
	{
		Player curr_player = players.get(current_player_position);
		curr_player.marryCards();
	}
	
	public void setCurrentPlayerPosition(int new_pos)
	{
		current_player_position = new_pos;
	}
	
	public int getCurrentPlayerPosition()
	{
		return current_player_position;
	}
	
	public int numberOfMarriedCards()
	{
		int number_of_married_cards = 0;
		int size = orig_players.size();
		for (int i = 0; i < size; i++)
		{
			Player curr_player = orig_players.get(i);
			number_of_married_cards += curr_player.numberOfMarriedCards();
		}
		return number_of_married_cards;
	}
}