package game;

public class HiddenPlayer implements PlayerType
{
	private static final long serialVersionUID = -8376298431267274824L;
	private String player_name;
	private int num_cards;
	
	public HiddenPlayer(String player_name, int num_cards)
	{
		this.player_name = player_name;
		this.num_cards = num_cards;
	}
	
	@Override
	public String getPlayerName()
	{
		return player_name;
	}
	
	@Override
	public int numberOfCards()
	{
		return num_cards;
	}
}