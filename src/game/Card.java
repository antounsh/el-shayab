package game;

import java.io.Serializable;

import javafx.scene.image.Image;

public class Card implements Serializable
{
	private static final long serialVersionUID = 4753788027669903921L;
	// 1-10 11=J 12=Q 13=K
	private int number;
	// S=Spades H=Hearts C=Clubs D=Diamonds
	private char suit;
	private transient Image front_image;
	private transient Image back_image;
	
	public Card(int card_number, char card_suit)
	{
		number = card_number;
		suit = card_suit;
		front_image = null;
		back_image = null;
	}
	
	public void setFrontImage(Image img)
	{
		front_image = img;
	}
	
	public Image getFrontImage()
	{
		return front_image;
	}
	
	public void setBackmage(Image img)
	{
		back_image = img;
	}
	
	public Image getBackImage()
	{
		return back_image;
	}
	
	public int getNumber()
	{
		return number;
	}
	
	public String getSuit()
	{
		if(suit == 'S')
			return "Spades";
		else if(suit == 'H')
			return "Hearts";
		else if(suit == 'C')
			return "Clubs";
		else
			return "Diamonds";
	}
}
