package game;

import java.io.Serializable;

public interface PlayerType extends Serializable
{
	public String getPlayerName();
	public int numberOfCards();
}
