package game;

import java.util.ArrayList;
import util.Random;

public class Player implements PlayerType
{
	private static final long serialVersionUID = -6921556827325976094L;
	private ArrayList<Card> hand;
	private String player_name;
	private int num_mcs;
	
	public Player(String playerName)
	{
		player_name = playerName;
		hand = new ArrayList<Card>();
		num_mcs = 0;
	}
	
	public Player(Player player)
	{
		player_name = player.getPlayerName();
		hand = player.getHand();
		num_mcs = player.numberOfMarriedCards();
	}
	
	@Override
	public String getPlayerName()
	{
		return player_name;
	}
	
	@Override
	public int numberOfCards()
	{
		return hand.size();
	}
	
	public int numberOfMarriedCards()
	{
		return num_mcs;
	}
	
	public void setPlayerName(String playerName)
	{
		player_name = playerName;
	}
	
	public void addCard(Card card)
	{
		hand.add(card);
	}
	
	public Card removeCard(int pos)
	{
		return hand.remove(pos);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Card> getHand()
	{
		return (ArrayList<Card>) hand.clone();
	}
	
	public void shuffleHand()
	{
		Random number_generator = Random.getRandomNumberGenerator();
		ArrayList<Card> shuffled_hand = new ArrayList<Card>();
		int size = hand.size();
		
		for (int i = 0; i < size; i++)
		{
			int random_number = number_generator.randomInt(0, (hand.size() - 1));
			Card curr_card = hand.remove(random_number);
			shuffled_hand.add(curr_card);
		}

		hand = shuffled_hand;
	}
	
	public void marryCards()
	{
		for (int i = 0; i < hand.size(); i++)
		{
			Card curr_card = hand.get(i);
			int curr_card_pos = i;
			for (int j = 0; j < hand.size(); j++)
			{
				if (curr_card_pos == j)
					continue;
				
				Card compare_card = hand.get(j);
				if (curr_card.getNumber() == compare_card.getNumber())
				{
					if (i > j)
					{
						hand.remove(i);
						hand.remove(j);
					}
					else
					{
						hand.remove(j);
						hand.remove(i); 
					}
					num_mcs += 2;
					i = 0;
					break;
				}
			}
		}
	}
}