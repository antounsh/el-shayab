package util;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.prefs.Preferences;

import org.ini4j.Ini;
import org.ini4j.IniPreferences;
import org.ini4j.Profile.Section;

public class SettingsLoader
{
	private Ini game_ini;
	private File user_config;
	private File game_config;
	
	public SettingsLoader(String user_config_loc, String game_config_loc)
	{
		game_ini = null;
		user_config = new File(user_config_loc);
		game_config = new File(game_config_loc);
	}
	
	// returns empty string if resolution does not exist
	public String getFXMLFileName()
	{
		double[] selected_reso = readSettingsFromFile();
		if (selected_reso[0] == -1) return "";
		
		String fxml_file_name = "resources/game";
		try
		{
			Preferences game_prefs = new IniPreferences(game_ini);
			String[] resolutions = game_prefs.node("Settings").get("Resolutions", null).split(",");
			int num_resolutions = resolutions.length;
			for (int i = 0; i < num_resolutions; i++)
			{
				String[] curr_resolution = resolutions[i].split("x");
				double width = Double.parseDouble(curr_resolution[0]);
				double height = Double.parseDouble(curr_resolution[1]);
				
				if (selected_reso[0] == width && selected_reso[1] == height) fxml_file_name += (i + 1) + ".fxml";
			}
		} catch (Exception e)
		{
			fxml_file_name = "";
		}
		return fxml_file_name;
	}
	
	// Returns empty string if cannot read file
	public String getPlayerName()
	{
		String p_name = "";
		try
		{
			Ini user_ini = new Ini(user_config);
			Section settings = user_ini.get("Settings");
			p_name = settings.get("PlayerName");
		} catch (Exception e)
		{
		}
		return p_name;
	}
	
	// Returns empty string if cannot read file
	public String getSelectedResolution()
	{
		String selected_reso = "";
		try
		{
			Ini user_ini = new Ini(user_config);
			Section settings = user_ini.get("Settings");
			selected_reso = settings.get("Resolution");
		} catch (Exception e)
		{
		}
		return selected_reso;
	}
	
	public ArrayList<String> getValidResolutions()
	{
		ArrayList<String> valid_resolutions = new ArrayList<String>();
		try
		{
			if (game_ini == null) game_ini = new Ini(game_config);
			double[] screen_size = getScreenSize();
			
			Preferences game_prefs = new IniPreferences(game_ini);
			String[] resolutions = game_prefs.node("Settings").get("Resolutions", null).split(",");
			for (String curr_reso : resolutions)
			{
				String[] temp = curr_reso.split("x");
				double width = Double.parseDouble(temp[0].trim());
				double height = Double.parseDouble(temp[1].trim());
				
				if (width < screen_size[0] && height < screen_size[1]) valid_resolutions.add(((int) width) + "x" + ((int) height));
			}
			if (valid_resolutions.isEmpty()) valid_resolutions = null;
		} catch (Exception e)
		{
			valid_resolutions = null;
		}
		
		return valid_resolutions;
	}
	
	public boolean writePlayerNameToFile(String player_name)
	{
		try
		{
			Ini user_ini = new Ini(user_config);
			Section settings = user_ini.get("Settings");
			settings.put("PlayerName", player_name);
			user_ini.store();
		} catch (Exception e)
		{
			return false;
		}
		return true;
	}
	
	public boolean writeScreenResolutionToFile(String screen_resolution)
	{
		try
		{
			Ini user_ini = new Ini(user_config);
			Section settings = user_ini.get("Settings");
			settings.put("Resolution", screen_resolution);
			user_ini.store();
		} catch (Exception e)
		{
			return false;
		}
		return true;
	}
	
	private double[] readSettingsFromFile()
	{
		double[] selected_reso = new double[2];
		try
		{
			game_ini = new Ini(game_config);
			Preferences prefs = new IniPreferences(new Ini(user_config));
			
			String[] resolutions = prefs.node("Settings").get("Resolution", null).split("x");
			selected_reso[0] = Double.parseDouble(resolutions[0].trim());
			selected_reso[1] = Double.parseDouble(resolutions[1].trim());
			
			if (!isResolutionWithinRange(selected_reso)) throw new Exception("Not a valid resolution.");
		} catch (Exception e)
		{
			selected_reso = writeScreenResolutionToFile();
		}
		
		return selected_reso;
	}
	
	// Ensures the selected resolution is not larger than the screen size and is a valid option in the game config
	private boolean isResolutionWithinRange(double[] resolution)
	{
		double[] screen_size = getScreenSize();
		if (resolution[0] > screen_size[0] || resolution[1] > screen_size[1]) return false;
		
		Preferences game_prefs = new IniPreferences(game_ini);
		String[] resolutions = game_prefs.node("Settings").get("Resolutions", null).split(",");
		for (String curr_reso : resolutions)
		{
			String[] temp = curr_reso.split("x");
			double width = Double.parseDouble(temp[0].trim());
			double height = Double.parseDouble(temp[1].trim());
			
			if (resolution[0] == width && resolution[1] == height) return true;
		}
		return false;
	}
	
	// returns {-1.0, -1.0} if no optimal resolution is found
	private double[] getOptimalScreenResolution()
	{
		Preferences game_prefs = new IniPreferences(game_ini);
		String[] resolutions = game_prefs.node("Settings").get("Resolutions", null).split(",");
		double[] optimal_reso = {-1.0, -1.0};
		double[] screen_size = getScreenSize();
		for (String curr_reso : resolutions)
		{
			String[] temp = curr_reso.split("x");
			double width = Double.parseDouble(temp[0].trim());
			double height = Double.parseDouble(temp[1].trim());
			
			if (screen_size[0] > width && screen_size[1] > height && width > optimal_reso[0] && height > optimal_reso[1])
			{
				optimal_reso[0] = width;
				optimal_reso[1] = height;
			}
		}
		return optimal_reso;
	}
	
	private double[] writeScreenResolutionToFile()
	{
		double[] screen_resolution = {-1.0, 1.0};
		try
		{
			screen_resolution = getOptimalScreenResolution();
			if (screen_resolution[0] == -1.0) return screen_resolution;
			
			game_ini.store(user_config);
			Ini user_ini = new Ini(user_config);
			
			Section settings = user_ini.get("Settings");
			settings.remove("Resolutions");
			settings.add("Resolution", (((int) screen_resolution[0]) + "x" + ((int) screen_resolution[1])));
			user_ini.store();
		} catch (Exception e)
		{
			screen_resolution[0] = -1.0;
		}
		
		return screen_resolution;
	}
	
	private double[] getScreenSize()
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double[] screen_size = {screenSize.getWidth(), screenSize.getHeight()};
		return screen_size;
	}
}
