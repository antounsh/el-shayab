package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.URL;

public abstract class NetworkChecker
{
    public static String getIP() throws Exception 
    {
        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = null;
        try 
        {
            in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            String ip = in.readLine();
            return ip;
        } finally 
        {
            if (in != null) 
            {
                try 
                {
                    in.close();
                } catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    // -1 is no open port found or low_range > high_range
    // if low_range is 0 then a random port is selected
    public static int getOpenPort(int low_range, int high_range)
    {
    	for (int port = low_range; port <= high_range; port++)
    	{
    		ServerSocket ss = null;
            DatagramSocket ds = null;
            
            try 
            {
                ss = new ServerSocket(port);
                ss.setReuseAddress(true);
                ds = new DatagramSocket(port);
                ds.setReuseAddress(true);
                return port;
            } catch (IOException e) 
            {
            } finally 
            {
                if (ds != null)
                    ds.close();

                if (ss != null) 
                {
                    try 
                    {
                        ss.close();
                    } catch (IOException e) 
                    {
                    }
                }
            }
    	}
    	
        return -1;
    }
}